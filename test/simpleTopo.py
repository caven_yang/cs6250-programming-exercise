from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
import mininet

def main():
        topo=Topo()
	s1=topo.addSwitch('s1')
	h1=topo.addHost('h1')
	h2=topo.addHost('h2')
	topo.addLink(h1,s1)
	topo.addLink(h2,s1)
        net=Mininet(topo=topo)
        net.start()
        mininet.cli.CLI(net)
        net.stop()

if __name__=='__main__':
    main()
