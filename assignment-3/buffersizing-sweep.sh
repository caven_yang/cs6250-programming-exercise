#!/bin/bash

# Exit on any failure
set -e

# Check for uninitialized variables
set -o nounset

ctrlc() {
	killall -9 python
	mn -c
	exit
}

trap ctrlc SIGINT

start=`date`
exptid=`date +%b%d-%H:%M`

echo "start is $start"
echo "exptid is $exptid"


rootdir=buffersizing-$exptid
plotpath=util
iperf=~/iperf-patched/src/iperf

echo "rootdir is $rootdir"


# TODO: change the interface for which queue size is adjusted
# should be <switch-name>-eth3
iface=s1-eth3

for run in 2; do
echo "run is $run"
for flows_per_host in 1 2 5 10 50 100 150 200 250 300 400; do
#for flows_per_host in 200 300 350; do
    python kill_iperf.py
    mn -c ##I added this
    echo "flows per host=$flows_per_host"
    dir=$rootdir/nf$flows_per_host-r$run
    echo "python buffersizing.py parameters:"
    echo "--bw-host 1000"
    echo "--bw-net 62.5"
    echo "--delay 43.5"
    echo "--dir $dir"
    echo "--nflows $flows_per_host"
    echo "--iperf $iperf"
    python buffersizing.py --bw-host 1000 \
	--bw-net 62.5 \
	--delay 43.5 \
	--dir $dir \
	--nflows $flows_per_host \
	--iperf $iperf

    python $plotpath/plot_queue.py -f $dir/qlen_$iface.txt -o $dir/q.png
    python $plotpath/plot_tcpprobe.py -f $dir/tcp_probe.txt -o $dir/cwnd.png --histogram
    rm -f $dir/tcp_probe.txt
done
done

cat $rootdir/*/result.txt | sort -n -k 1
cat $rootdir/*/result.txt | sort -n -k 1 > $rootdir/buffer-size-result.txt
python plot-results.py --dir $rootdir -o $rootdir/result.png
echo "Started at" $start
echo "Ended at" `date`
