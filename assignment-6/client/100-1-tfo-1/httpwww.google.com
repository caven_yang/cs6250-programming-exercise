[24934:24939:8736114857:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[24934:24942:8738986751:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[24934:24942:8739196850:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[24934:24942:8739406026:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	1
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	3463
c:URLRequestCount:	9
c:disk_cache.miss:	9
c:HttpNetworkTransaction.Count:	9
c:tcp.connect:	11
c:tcp.write_bytes:	4090
c:tcp.read_bytes:	752690
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  102.100 |  3707.181 |  3605.081 |   0 | www.google.com:80 ->  10.0.0.5:8000
  102.361 |   102.361 |     0.000 |   1 | www.google.com:80 ->  nil
 1394.372 |  1394.604 |     0.232 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
 1394.600 |  1394.600 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
 1402.356 |  1402.356 |     0.000 |   1 | www.google.com:80 ->  nil
 1402.381 |  1402.381 |     0.000 |   1 | www.google.com:80 ->  nil
 1454.666 |  1454.666 |     0.000 |   1 | www.google.com:80 ->  nil
 1454.695 |  1454.695 |     0.000 |   1 | www.google.com:80 ->  nil
 1635.033 |  1635.063 |     0.030 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
 1635.060 |  1635.060 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 2750.488 |  3172.293 |   421.805 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 2750.574 |  2750.574 |     0.000 |   1 | apis.google.com:443 ->  nil
 2750.579 |  2750.579 |     0.000 |   1 | apis.google.com:443 ->  nil
 2962.620 |  2962.620 |     0.000 |   1 | apis.google.com:443 ->  nil
 2962.703 |  2962.703 |     0.000 |   1 | apis.google.com:443 ->  nil
 2962.717 |  2962.717 |     0.000 |   1 | apis.google.com:443 ->  nil
 3172.191 |  3172.191 |     0.000 |   1 | apis.google.com:443 ->  nil
 3172.275 |  3172.275 |     0.000 |   1 | apis.google.com:443 ->  nil
 3172.289 |  3172.289 |     0.000 |   1 | apis.google.com:443 ->  nil
 3675.932 |  3675.932 |     0.000 |   1 | www.google.com:80 ->  nil
 3675.961 |  3675.961 |     0.000 |   1 | www.google.com:80 ->  nil
 3684.761 |  3684.761 |     0.000 |   1 | www.google.com:80 ->  nil
 3684.787 |  3684.787 |     0.000 |   1 | www.google.com:80 ->  nil
 3707.153 |  3707.153 |     0.000 |   1 | www.google.com:80 ->  nil
 3707.179 |  3707.179 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  101.946 |  1428.881 |  1326.935 | http://www.google.com/
 1402.337 |  1611.413 |   209.076 | http://www.google.com/images/srpr/logo11w.png
 1394.352 |  2007.271 |   612.919 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
 1635.012 |  2691.028 |  1056.016 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 2750.466 |  3382.827 |   632.361 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
 1454.642 |  3494.939 |  2040.297 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 5 samples, average = 147621.2 (flags = 0x1)
0       ... 
12985   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
14032   ... 
52461   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
56692   ... 
133075  ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
143809  ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
155408  ... 
364793  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
394217  ... 

Histogram: Net.ConnectionTypeCount3 recorded 19 samples, average = 2.9 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 57.9%)
1  ... 
7  ----------------------------------------------------O                     (8 = 42.1%) {57.9%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpJob.TotalTime recorded 6 samples, average = 978.2 (flags = 0x1)
0     ... 
190   ------------------------------------O                                     (1 = 16.7%) {0.0%}
226   ... 
533   ------------------------------------------------------------------------O (2 = 33.3%) {16.7%}
633   ... 
894   ------------------------------------O                                     (1 = 16.7%) {50.0%}
1062  O                                                                         (0 = 0.0%) {66.7%}
1262  ------------------------------------O                                     (1 = 16.7%) {66.7%}
1500  O                                                                         (0 = 0.0%) {83.3%}
1782  ------------------------------------O                                     (1 = 16.7%) {83.3%}
2117  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 632.0 (flags = 0x1)
0    ... 
533  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
633  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 5 samples, average = 1047.4 (flags = 0x1)
0     ... 
190   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
226   ... 
533   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
633   ... 
894   ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
1062  O                                                                         (0 = 0.0%) {60.0%}
1262  ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
1500  O                                                                         (0 = 0.0%) {80.0%}
1782  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
2117  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 5 samples, average = 1047.4 (flags = 0x1)
0     ... 
190   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
226   ... 
533   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
633   ... 
894   ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
1062  O                                                                         (0 = 0.0%) {60.0%}
1262  ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
1500  O                                                                         (0 = 0.0%) {80.0%}
1782  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
2117  ... 

Histogram: Net.HttpResponseCode recorded 6 samples, average = 200.0 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (6 = 100.0%) {0.0%}
201  ... 

Histogram: Net.HttpSocketType recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 7 samples, average = 268.7 (flags = 0x1)
0    ... 
171  ------------------------------------------------------------------------O (4 = 57.1%) {0.0%}
210  ------------------------------------O                                     (2 = 28.6%) {57.1%}
258  ... 
585  ------------------O                                                       (1 = 14.3%) {85.7%}
718  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 8 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (8 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.Transaction_Latency_Total recorded 5 samples, average = 1046.4 (flags = 0x1)
0     ... 
186   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
211   ... 
572   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
648   ... 
941   ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
1065  O                                                                         (0 = 0.0%) {60.0%}
1206  ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
1365  ... 
1981  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
2243  ...[24934:24939:8739915735:FATAL:url_request_context.cc(121)] Check failed: false. Leaked 3 URLRequest(s). First URL: http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf..
 

Histogram: Net.Transaction_Latency_Total_New_Connection recorded 5 samples, average = 1046.4 (flags = 0x1)
0     ... 
186   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
211   ... 
572   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
648   ... 
941   ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
1065  O                                                                         (0 = 0.0%) {60.0%}
1206  ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
1365  ... 
1981  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
2243  ... 

Histogram: Net.Transaction_Latency_b recorded 5 samples, average = 1046.2 (flags = 0x1)
0     ... 
186   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
211   ... 
572   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
648   ... 
941   ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
1065  O                                                                         (0 = 0.0%) {60.0%}
1206  ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
1365  ... 
1981  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
2243  ... 


</queries>
Received signal 6
 [0x000000531f1e] base::debug::StackTrace::StackTrace()
 [0x000000532418] base::debug::(anonymous namespace)::StackDumpSignalHandler()
 [0x7f2862164bd0] <unknown>
 [0x7f286118e037] gsignal
 [0x7f2861191698] abort
 [0x00000056c349] base::debug::BreakDebugger()
 [0x0000005405cd] logging::LogMessage::~LogMessage()
 [0x000001c2eb7e] net::URLRequestContext::AssertNoURLRequests()
 [0x000001c2ebc8] net::URLRequestContext::~URLRequestContext()
 [0x000001aec899] TestShellRequestContext::~TestShellRequestContext()
 [0x000001ae3c1f] (anonymous namespace)::IOThread::CleanUp()
 [0x00000055eafd] base::Thread::ThreadMain()
 [0x00000055e361] base::(anonymous namespace)::ThreadFunc()
 [0x7f286215cf8e] start_thread
 [0x7f2861250e1d] clone
  r8: 000000000204024d  r9: 00007f285a9bab26 r10: 0000000000000008 r11: 0000000000000206
 r12: 0000000000000000 r13: 00007f2861519848 r14: 00007fff6eb1f280 r15: 0000000000001000
  di: 0000000000006166  si: 000000000000616b  bp: 00007f2861d3c4c0  bx: 00007f285a9bb620
  dx: 0000000000000006  ax: 0000000000000000  cx: ffffffffffffffff  sp: 00007f285a9badc8
  ip: 00007f286118e037 efl: 0000000000000206 cgf: 0000000000000033 erf: 0000000000000000
 trp: 0000000000000000 msk: 0000000000000000 cr2: 0000000000000000
