[23363:23368:8670903318:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[23363:23371:8672018577:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[23363:23371:8672047528:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[23363:23371:8672075198:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	1
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	1260
c:URLRequestCount:	9
c:disk_cache.miss:	9
c:HttpNetworkTransaction.Count:	9
c:tcp.connect:	11
c:tcp.write_bytes:	4090
c:tcp.read_bytes:	850803
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  113.739 |  1498.409 |  1384.670 |   0 | www.google.com:80 ->  10.0.0.5:8000
  113.972 |   113.972 |     0.000 |   1 | www.google.com:80 ->  nil
  446.384 |   446.384 |     0.000 |   1 | www.google.com:80 ->  nil
  446.417 |   446.417 |     0.000 |   1 | www.google.com:80 ->  nil
  452.534 |   452.565 |     0.031 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
  452.562 |   452.562 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
  492.636 |   492.636 |     0.000 |   1 | www.google.com:80 ->  nil
  492.661 |   492.661 |     0.000 |   1 | www.google.com:80 ->  nil
  716.268 |   716.339 |     0.071 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
  716.332 |   716.332 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 1123.038 |  1247.769 |   124.731 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 1123.118 |  1123.118 |     0.000 |   1 | apis.google.com:443 ->  nil
 1123.124 |  1123.124 |     0.000 |   1 | apis.google.com:443 ->  nil
 1217.838 |  1217.838 |     0.000 |   1 | apis.google.com:443 ->  nil
 1217.889 |  1217.889 |     0.000 |   1 | apis.google.com:443 ->  nil
 1217.896 |  1217.896 |     0.000 |   1 | apis.google.com:443 ->  nil
 1247.386 |  1247.386 |     0.000 |   1 | apis.google.com:443 ->  nil
 1247.749 |  1247.749 |     0.000 |   1 | apis.google.com:443 ->  nil
 1247.765 |  1247.765 |     0.000 |   1 | apis.google.com:443 ->  nil
 1464.358 |  1464.358 |     0.000 |   1 | www.google.com:80 ->  nil
 1464.387 |  1464.387 |     0.000 |   1 | www.google.com:80 ->  nil
 1473.126 |  1473.126 |     0.000 |   1 | www.google.com:80 ->  nil
 1473.152 |  1473.152 |     0.000 |   1 | www.google.com:80 ->  nil
 1498.375 |  1498.375 |     0.000 |   1 | www.google.com:80 ->  nil
 1498.407 |  1498.407 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  113.578 |   487.546 |   373.968 | http://www.google.com/
  446.364 |   529.780 |    83.416 | http://www.google.com/images/srpr/logo11w.png
  452.514 |   614.487 |   161.973 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
  716.088 |  1070.410 |   354.322 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 1123.018 |  1276.157 |   153.139 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
  492.616 |  1295.780 |   803.164 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 1464.328 |  1686.258 |   221.930 | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf.
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
          OK |       text/html |         | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf. -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 6 samples, average = 135784.2 (flags = 0x1)
0       ... 
12985   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
14032   ... 
52461   ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
56692   ... 
71547   ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
77318   ... 
133075  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
143809  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
155408  ... 
364793  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
394217  ... 

Histogram: Net.ConnectionTypeCount3 recorded 19 samples, average = 2.9 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 57.9%)
1  ... 
7  ----------------------------------------------------O                     (8 = 42.1%) {57.9%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpJob.TotalTime recorded 7 samples, average = 306.4 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 14.3%) {0.0%}
96   ... 
135  ------------------------------------O                                     (1 = 14.3%) {14.3%}
160  ------------------------------------O                                     (1 = 14.3%) {28.6%}
190  ------------------------------------O                                     (1 = 14.3%) {42.9%}
226  ... 
318  ------------------------------------------------------------------------O (2 = 28.6%) {57.1%}
378  ... 
752  ------------------------------------O                                     (1 = 14.3%) {85.7%}
894  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 152.0 (flags = 0x1)
0    ... 
135  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
160  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 6 samples, average = 332.2 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 16.7%) {0.0%}
96   ... 
160  ------------------------------------O                                     (1 = 16.7%) {16.7%}
190  ------------------------------------O                                     (1 = 16.7%) {33.3%}
226  ... 
318  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
378  ... 
752  ------------------------------------O                                     (1 = 16.7%) {83.3%}
894  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 6 samples, average = 332.2 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 16.7%) {0.0%}
96   ... 
160  ------------------------------------O                                     (1 = 16.7%) {16.7%}
190  ------------------------------------O                                     (1 = 16.7%) {33.3%}
226  ... 
318  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
378  ... 
752  ------------------------------------O                                     (1 = 16.7%) {83.3%}
894  ... 

Histogram: Net.HttpResponseCode recorded 7 samples, average = 200.0 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (7 = 100.0%) {0.0%}
201  ... 

Histogram: Net.HttpSocketType recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 8 samples, average = 76.8 (flags = 0x1)
0    ... 
27   ------------------------------------------------O                         (2 = 25.0%) {0.0%}
33   ------------------------O                                                 (1 = 12.5%) {25.0%}
41   ... 
75   ------------------------------------------------------------------------O (3 = 37.5%) {37.5%}
92   ... 
139  ------------------------------------------------O                         (2 = 25.0%) {75.0%}
171  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 9 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (9 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.Transaction_Latency_Total recorded 6 samples, average = 330.8 (flags = 0x1)
0    ... 
78   ------------------------------------O                                     (1 = 16.7%) {0.0%}
88   ... 
145  ------------------------------------O                                     (1 = 16.7%) {16.7%}
164  ... 
211  ------------------------------------O                                     (1 = 16.7%) {33.3%}
239  ... 
348  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
394  ... 
734  ------------------------------------O                                     (1 = 16.7%) {83.3%}
831  ... 

Histogram: Net.Transaction_Latency_Total_New_Connection recorded 6 samples, average = 330.8 (flags = 0x1)
0    ... 
78   -------------[23363:23368:8672493906:FATAL:url_request_context.cc(121)] Check failed: false. Leaked 2 URLRequest(s). First URL: http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw.
-----------------------O                                     (1 = 16.7%) {0.0%}
88   ... 
145  ------------------------------------O                                     (1 = 16.7%) {16.7%}
164  ... 
211  ------------------------------------O                                     (1 = 16.7%) {33.3%}
239  ... 
348  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
394  ... 
734  ------------------------------------O                                     (1 = 16.7%) {83.3%}
831  ... 

Histogram: Net.Transaction_Latency_b recorded 6 samples, average = 330.5 (flags = 0x1)
0    ... 
78   ------------------------------------O                                     (1 = 16.7%) {0.0%}
88   ... 
145  ------------------------------------O                                     (1 = 16.7%) {16.7%}
164  ... 
211  ------------------------------------O                                     (1 = 16.7%) {33.3%}
239  ... 
348  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
394  ... 
734  ------------------------------------O                                     (1 = 16.7%) {83.3%}
831  ... 


</queries>
Received signal 6
 [0x000000531f1e] base::debug::StackTrace::StackTrace()
 [0x000000532418] base::debug::(anonymous namespace)::StackDumpSignalHandler()
 [0x7f0fcb73dbd0] <unknown>
 [0x7f0fca767037] gsignal
 [0x7f0fca76a698] abort
 [0x00000056c349] base::debug::BreakDebugger()
 [0x0000005405cd] logging::LogMessage::~LogMessage()
 [0x000001c2eb7e] net::URLRequestContext::AssertNoURLRequests()
 [0x000001c2ebc8] net::URLRequestContext::~URLRequestContext()
 [0x000001aec899] TestShellRequestContext::~TestShellRequestContext()
 [0x000001ae3c1f] (anonymous namespace)::IOThread::CleanUp()
 [0x00000055eafd] base::Thread::ThreadMain()
 [0x00000055e361] base::(anonymous namespace)::ThreadFunc()
 [0x7f0fcb735f8e] start_thread
 [0x7f0fca829e1d] clone
  r8: 000000000204024d  r9: 00007f0fc3f93b26 r10: 0000000000000008 r11: 0000000000000202
 r12: 0000000000000000 r13: 00007f0fcaaf2848 r14: 00007fff3c8dde30 r15: 0000000000001000
  di: 0000000000005b43  si: 0000000000005b48  bp: 00007f0fcb3154c0  bx: 00007f0fc3f94620
  dx: 0000000000000006  ax: 0000000000000000  cx: ffffffffffffffff  sp: 00007f0fc3f93dc8
  ip: 00007f0fca767037 efl: 0000000000000202 cgf: 0000000000000033 erf: 0000000000000000
 trp: 0000000000000000 msk: 0000000000000000 cr2: 0000000000000000
