[24162:24167:8701773838:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[24162:24170:8703372003:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[24162:24170:8703478320:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[24162:24170:8703584689:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	1
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	2104
c:URLRequestCount:	9
c:disk_cache.miss:	9
c:HttpNetworkTransaction.Count:	9
c:tcp.connect:	11
c:tcp.write_bytes:	4090
c:tcp.read_bytes:	770186
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  118.997 |  2348.192 |  2229.195 |   0 | www.google.com:80 ->  10.0.0.5:8000
  119.230 |   119.230 |     0.000 |   1 | www.google.com:80 ->  nil
  715.012 |   715.062 |     0.050 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
  715.059 |   715.059 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
  748.928 |   748.928 |     0.000 |   1 | www.google.com:80 ->  nil
  748.956 |   748.956 |     0.000 |   1 | www.google.com:80 ->  nil
  788.316 |   788.316 |     0.000 |   1 | www.google.com:80 ->  nil
  788.342 |   788.342 |     0.000 |   1 | www.google.com:80 ->  nil
  961.245 |   961.276 |     0.031 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
  961.273 |   961.273 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 1598.248 |  1813.858 |   215.610 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 1598.340 |  1598.340 |     0.000 |   1 | apis.google.com:443 ->  nil
 1598.346 |  1598.346 |     0.000 |   1 | apis.google.com:443 ->  nil
 1707.731 |  1707.731 |     0.000 |   1 | apis.google.com:443 ->  nil
 1707.818 |  1707.818 |     0.000 |   1 | apis.google.com:443 ->  nil
 1707.832 |  1707.832 |     0.000 |   1 | apis.google.com:443 ->  nil
 1813.814 |  1813.814 |     0.000 |   1 | apis.google.com:443 ->  nil
 1813.851 |  1813.851 |     0.000 |   1 | apis.google.com:443 ->  nil
 1813.856 |  1813.856 |     0.000 |   1 | apis.google.com:443 ->  nil
 2315.478 |  2315.478 |     0.000 |   1 | www.google.com:80 ->  nil
 2315.504 |  2315.504 |     0.000 |   1 | www.google.com:80 ->  nil
 2326.029 |  2326.029 |     0.000 |   1 | www.google.com:80 ->  nil
 2326.055 |  2326.055 |     0.000 |   1 | www.google.com:80 ->  nil
 2348.164 |  2348.164 |     0.000 |   1 | www.google.com:80 ->  nil
 2348.190 |  2348.190 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  118.846 |   742.362 |   623.516 | http://www.google.com/
  748.905 |   859.324 |   110.419 | http://www.google.com/images/srpr/logo11w.png
  714.982 |  1029.044 |   314.062 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
  961.223 |  1536.370 |   575.147 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 1598.224 |  1921.893 |   323.669 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
  788.294 |  2144.286 |  1355.992 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 5 samples, average = 147621.2 (flags = 0x1)
0       ... 
12985   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
14032   ... 
52461   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
56692   ... 
133075  ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
143809  ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
155408  ... 
364793  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
394217  ... 

Histogram: Net.ConnectionTypeCount3 recorded 19 samples, average = 2.9 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 57.9%)
1  ... 
7  ----------------------------------------------------O                     (8 = 42.1%) {57.9%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpJob.TotalTime recorded 6 samples, average = 550.3 (flags = 0x1)
0     ... 
96    ------------------------------------O                                     (1 = 16.7%) {0.0%}
114   ... 
318   ------------------------------------------------------------------------O (2 = 33.3%) {16.7%}
378   ... 
533   ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
633   ... 
1262  ------------------------------------O                                     (1 = 16.7%) {83.3%}
1500  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 322.0 (flags = 0x1)
0    ... 
318  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
378  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 5 samples, average = 596.0 (flags = 0x1)
0     ... 
96    ------------------------------------O                                     (1 = 20.0%) {0.0%}
114   ... 
318   ------------------------------------O                                     (1 = 20.0%) {20.0%}
378   ... 
533   ------------------------------------------------------------------------O (2 = 40.0%) {40.0%}
633   ... 
1262  ------------------------------------O                                     (1 = 20.0%) {80.0%}
1500  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 5 samples, average = 596.0 (flags = 0x1)
0     ... 
96    ------------------------------------O                                     (1 = 20.0%) {0.0%}
114   ... 
318   ------------------------------------O                                     (1 = 20.0%) {20.0%}
378   ... 
533   ------------------------------------------------------------------------O (2 = 40.0%) {40.0%}
633   ... 
1262  ------------------------------------O                                     (1 = 20.0%) {80.0%}
1500  ... 

Histogram: Net.HttpResponseCode recorded 6 samples, average = 200.0 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (6 = 100.0%) {0.0%}
201  ... 

Histogram: Net.HttpSocketType recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 7 samples, average = 137.9 (flags = 0x1)
0    ... 
92   ------------------------------------------------------------------------O (6 = 85.7%) {0.0%}
113  ... 
317  ------------O                                                             (1 = 14.3%) {85.7%}
389  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 8 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (8 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.Transaction_Latency_Total recorded 5 samples, average = 594.0 (flags = 0x1)
0     ... 
100   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
113   ... 
307   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
348   ... 
505   ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
572   ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
648   ... 
1206  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
1365  ... 

Histogram: Net.Transaction_Latency_Total_New_Connection recorded 5 samples, average = 594.0 (flags = 0x1)
0     ... 
100   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
113   ... 
307   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
348   ... 
505   ------------------------------------------------------------------------O (1 = 20.0%) {40.0%}
572   ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
648   ... 
1206  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
1365  ... 

Histogram: Net.Transaction_Latency_b recorded 5 samples, average = 593.6 (flags = 0x1)
0     ... 
100   ------------------------------------------------------------------------O (1 = 20.0%) {0.0%}
113   ... 
307   ------------------------------------------------------------------------O (1 = 20.0%) {20.0%}
348   ... 
505   ------------------------------------------------------------------------O[24162:24167:8704197124:FATAL:url_request_context.cc(121)] Check failed: false. Leaked 3 URLRequest(s). First URL: http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw.
 (1 = 20.0%) {40.0%}
572   ------------------------------------------------------------------------O (1 = 20.0%) {60.0%}
648   ... 
1206  ------------------------------------------------------------------------O (1 = 20.0%) {80.0%}
1365  ... 


</queries>
Received signal 6
 [0x000000531f1e] base::debug::StackTrace::StackTrace()
 [0x000000532418] base::debug::(anonymous namespace)::StackDumpSignalHandler()
 [0x7f6778b5bbd0] <unknown>
 [0x7f6777b85037] gsignal
 [0x7f6777b88698] abort
 [0x00000056c349] base::debug::BreakDebugger()
 [0x0000005405cd] logging::LogMessage::~LogMessage()
 [0x000001c2eb7e] net::URLRequestContext::AssertNoURLRequests()
 [0x000001c2ebc8] net::URLRequestContext::~URLRequestContext()
 [0x000001aec899] TestShellRequestContext::~TestShellRequestContext()
 [0x000001ae3c1f] (anonymous namespace)::IOThread::CleanUp()
 [0x00000055eafd] base::Thread::ThreadMain()
 [0x00000055e361] base::(anonymous namespace)::ThreadFunc()
 [0x7f6778b53f8e] start_thread
 [0x7f6777c47e1d] clone
  r8: 000000000204024d  r9: 00007f67713b1b26 r10: 0000000000000008 r11: 0000000000000202
 r12: 0000000000000000 r13: 00007f6777f10848 r14: 00007fffc2d5d930 r15: 0000000000001000
  di: 0000000000005e62  si: 0000000000005e67  bp: 00007f67787334c0  bx: 00007f67713b2620
  dx: 0000000000000006  ax: 0000000000000000  cx: ffffffffffffffff  sp: 00007f67713b1dc8
  ip: 00007f6777b85037 efl: 0000000000000202 cgf: 0000000000000033 erf: 0000000000000000
 trp: 0000000000000000 msk: 0000000000000000 cr2: 0000000000000000
