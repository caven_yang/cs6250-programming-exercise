[23390:23395:8673503355:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[23390:23398:8674452842:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[23390:23398:8674477693:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[23390:23398:8674505158:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	1
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	1244
c:URLRequestCount:	9
c:disk_cache.miss:	9
c:HttpNetworkTransaction.Count:	9
c:tcp.connect:	11
c:tcp.write_bytes:	4090
c:tcp.read_bytes:	865201
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
   99.933 |  1501.893 |  1401.960 |   0 | www.google.com:80 ->  10.0.0.5:8000
  100.168 |   100.168 |     0.000 |   1 | www.google.com:80 ->  nil
  394.331 |   394.331 |     0.000 |   1 | www.google.com:80 ->  nil
  394.383 |   394.383 |     0.000 |   1 | www.google.com:80 ->  nil
  400.535 |   400.564 |     0.029 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
  400.561 |   400.561 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
  474.995 |   474.995 |     0.000 |   1 | www.google.com:80 ->  nil
  475.023 |   475.023 |     0.000 |   1 | www.google.com:80 ->  nil
  649.228 |   649.404 |     0.176 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
  649.393 |   649.393 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 1008.386 |  1062.018 |    53.632 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 1008.494 |  1008.494 |     0.000 |   1 | apis.google.com:443 ->  nil
 1008.500 |  1008.500 |     0.000 |   1 | apis.google.com:443 ->  nil
 1036.912 |  1036.912 |     0.000 |   1 | apis.google.com:443 ->  nil
 1036.949 |  1036.949 |     0.000 |   1 | apis.google.com:443 ->  nil
 1036.954 |  1036.954 |     0.000 |   1 | apis.google.com:443 ->  nil
 1061.923 |  1061.923 |     0.000 |   1 | apis.google.com:443 ->  nil
 1062.001 |  1062.001 |     0.000 |   1 | apis.google.com:443 ->  nil
 1062.014 |  1062.014 |     0.000 |   1 | apis.google.com:443 ->  nil
 1461.157 |  1461.157 |     0.000 |   1 | www.google.com:80 ->  nil
 1461.183 |  1461.183 |     0.000 |   1 | www.google.com:80 ->  nil
 1472.419 |  1472.419 |     0.000 |   1 | www.google.com:80 ->  nil
 1472.445 |  1472.445 |     0.000 |   1 | www.google.com:80 ->  nil
 1501.862 |  1501.862 |     0.000 |   1 | www.google.com:80 ->  nil
 1501.890 |  1501.890 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
   99.759 |   408.615 |   308.856 | http://www.google.com/
  394.312 |   479.089 |    84.777 | http://www.google.com/images/srpr/logo11w.png
  400.516 |   505.083 |   104.567 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
  649.180 |   956.289 |   307.109 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 1008.366 |  1090.840 |    82.474 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
  474.972 |  1284.570 |   809.598 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 1461.136 |  1710.232 |   249.096 | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf.
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
          OK |       text/html |         | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf. -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 6 samples, average = 135784.2 (flags = 0x1)
0       ... 
12985   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
14032   ... 
52461   ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
56692   ... 
71547   ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
77318   ... 
133075  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
143809  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
155408  ... 
364793  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
394217  ... 

Histogram: Net.ConnectionTypeCount3 recorded 19 samples, average = 2.9 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 57.9%)
1  ... 
7  ----------------------------------------------------O                     (8 = 42.1%) {57.9%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpJob.TotalTime recorded 7 samples, average = 275.6 (flags = 0x1)
0    ... 
81   ------------------------------------------------------------------------O (2 = 28.6%) {0.0%}
96   ------------------------------------O                                     (1 = 14.3%) {28.6%}
114  ... 
226  ------------------------------------O                                     (1 = 14.3%) {42.9%}
268  ------------------------------------------------------------------------O (2 = 28.6%) {57.1%}
318  ... 
752  ------------------------------------O                                     (1 = 14.3%) {85.7%}
894  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 81.0 (flags = 0x1)
0   ... 
81  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
96  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 6 samples, average = 308.0 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 16.7%) {0.0%}
96   ------------------------------------O                                     (1 = 16.7%) {16.7%}
114  ... 
226  ------------------------------------O                                     (1 = 16.7%) {33.3%}
268  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
318  ... 
752  ------------------------------------O                                     (1 = 16.7%) {83.3%}
894  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 6 samples, average = 308.0 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 16.7%) {0.0%}
96   ------------------------------------O                                     (1 = 16.7%) {16.7%}
114  ... 
226  ------------------------------------O                                     (1 = 16.7%) {33.3%}
268  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
318  ... 
752  ------------------------------------O                                     (1 = 16.7%) {83.3%}
894  ... 

Histogram: Net.HttpResponseCode recorded 7 samples, average = 200.0 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (7 = 100.0%) {0.0%}
201  ... 

Histogram: Net.HttpSocketType recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 8 samples, average = 44.9 (flags = 0x1)
0    ... 
22   ------------------------------------------------------------------------O (3 = 37.5%) {0.0%}
27   ------------------------------------------------------------------------O (3 = 37.5%) {37.5%}
33   ... 
75   ------------------------O                                                 (1 = 12.5%) {75.0%}
92   O                                                                         (0 = 0.0%) {87.5%}
113  ------------------------O                                                 (1 = 12.5%) {87.5%}
139  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 9 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (9 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.Transaction_Latency_Total recorded 6 samples, average = 307.0 (flags = 0x1)
0    ... 
78   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
88   O                                                                         (0 = 0.0%) {16.7%}
100  ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
113  ... 
239  ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
271  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
307  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
348  ... 
734  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
831  ... 

Hi[23390:23395:8675133704:FATAL:url_request_context.cc(121)] Check failed: false. Leaked 2 URLRequest(s). First URL: http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw.
stogram: Net.Transaction_Latency_Total_New_Connection recorded 6 samples, average = 307.0 (flags = 0x1)
0    ... 
78   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
88   O                                                                         (0 = 0.0%) {16.7%}
100  ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
113  ... 
239  ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
271  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
307  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
348  ... 
734  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
831  ... 

Histogram: Net.Transaction_Latency_b recorded 6 samples, average = 306.7 (flags = 0x1)
0    ... 
78   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
88   O                                                                         (0 = 0.0%) {16.7%}
100  ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
113  ... 
239  ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
271  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
307  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
348  ... 
734  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
831  ... 


</queries>
Received signal 6
 [0x000000531f1e] base::debug::StackTrace::StackTrace()
 [0x000000532418] base::debug::(anonymous namespace)::StackDumpSignalHandler()
 [0x7fbe0890fbd0] <unknown>
 [0x7fbe07939037] gsignal
 [0x7fbe0793c698] abort
 [0x00000056c349] base::debug::BreakDebugger()
 [0x0000005405cd] logging::LogMessage::~LogMessage()
 [0x000001c2eb7e] net::URLRequestContext::AssertNoURLRequests()
 [0x000001c2ebc8] net::URLRequestContext::~URLRequestContext()
 [0x000001aec899] TestShellRequestContext::~TestShellRequestContext()
 [0x000001ae3c1f] (anonymous namespace)::IOThread::CleanUp()
 [0x00000055eafd] base::Thread::ThreadMain()
 [0x00000055e361] base::(anonymous namespace)::ThreadFunc()
 [0x7fbe08907f8e] start_thread
 [0x7fbe079fbe1d] clone
  r8: 000000000204024d  r9: 00007fbe01165b26 r10: 0000000000000008 r11: 0000000000000202
 r12: 0000000000000000 r13: 00007fbe07cc4848 r14: 00007fffb9a7da00 r15: 0000000000001000
  di: 0000000000005b5e  si: 0000000000005b63  bp: 00007fbe084e74c0  bx: 00007fbe01166620
  dx: 0000000000000006  ax: 0000000000000000  cx: ffffffffffffffff  sp: 00007fbe01165dc8
  ip: 00007fbe07939037 efl: 0000000000000202 cgf: 0000000000000033 erf: 0000000000000000
 trp: 0000000000000000 msk: 0000000000000000 cr2: 0000000000000000
