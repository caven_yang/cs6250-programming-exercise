[21839:21844:8590533043:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[21839:21847:8592361673:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[21839:21847:8592614167:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[21839:21847:8592849783:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	0
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	4877
c:URLRequestCount:	11
c:disk_cache.miss:	11
c:HttpNetworkTransaction.Count:	11
c:tcp.connect:	13
c:tcp.write_bytes:	5707
c:tcp.read_bytes:	1248070
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  146.983 |  4595.656 |  4448.673 |   0 | www.google.com:80 ->  10.0.0.5:8000
  147.224 |   147.224 |     0.000 |   1 | www.google.com:80 ->  nil
  791.333 |   791.372 |     0.039 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
  791.370 |   791.370 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
  799.393 |   799.393 |     0.000 |   1 | www.google.com:80 ->  nil
  799.418 |   799.418 |     0.000 |   1 | www.google.com:80 ->  nil
  832.968 |   832.968 |     0.000 |   1 | www.google.com:80 ->  nil
  832.995 |   832.995 |     0.000 |   1 | www.google.com:80 ->  nil
 1035.358 |  1035.387 |     0.029 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
 1035.384 |  1035.384 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 1755.715 |  2217.400 |   461.685 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 1755.796 |  1755.796 |     0.000 |   1 | apis.google.com:443 ->  nil
 1755.802 |  1755.802 |     0.000 |   1 | apis.google.com:443 ->  nil
 1965.098 |  1965.098 |     0.000 |   1 | apis.google.com:443 ->  nil
 1965.181 |  1965.181 |     0.000 |   1 | apis.google.com:443 ->  nil
 1965.194 |  1965.194 |     0.000 |   1 | apis.google.com:443 ->  nil
 2217.354 |  2217.354 |     0.000 |   1 | apis.google.com:443 ->  nil
 2217.393 |  2217.393 |     0.000 |   1 | apis.google.com:443 ->  nil
 2217.399 |  2217.399 |     0.000 |   1 | apis.google.com:443 ->  nil
 2527.267 |  2527.267 |     0.000 |   1 | www.google.com:80 ->  nil
 2527.294 |  2527.294 |     0.000 |   1 | www.google.com:80 ->  nil
 2539.882 |  2539.882 |     0.000 |   1 | www.google.com:80 ->  nil
 2539.912 |  2539.912 |     0.000 |   1 | www.google.com:80 ->  nil
 2562.764 |  2562.764 |     0.000 |   1 | www.google.com:80 ->  nil
 2562.791 |  2562.791 |     0.000 |   1 | www.google.com:80 ->  nil
 4591.292 |  4591.292 |     0.000 |   1 | www.google.com:80 ->  nil
 4591.319 |  4591.319 |     0.000 |   1 | www.google.com:80 ->  nil
 4595.628 |  4595.628 |     0.000 |   1 | www.google.com:80 ->  nil
 4595.653 |  4595.653 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  146.822 |   825.980 |   679.158 | http://www.google.com/
  799.352 |  1017.611 |   218.259 | http://www.google.com/images/srpr/logo11w.png
  791.313 |  1208.793 |   417.480 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
 1035.336 |  1703.088 |   667.752 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
  832.945 |  1991.619 |  1158.674 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 1755.695 |  2453.019 |   697.324 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
 2527.244 |  3045.437 |   518.193 | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf.
 2539.858 |  4063.556 |  1523.698 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 2562.744 |  4576.499 |  2013.755 | http://www.google.com/textinputassistant/tia.png
 4595.605 |  4806.226 |   210.621 | http://www.google.com/gen_204?v=3&s=webhp&action=&e=4006,17259,4000116,4007661,4007830,4008067,4008133,4008142,4009033,4009352,4009565,4009641,4010806,4010858,4010899,4011228,4011258,4011679,4012373,4012504,4012508,4013374,4013414,4013591,4013723,4013747,4013787,4013823,4013967,4013979,4014016,4014092,4014431,4014515,4014637,4014649,4014671,4014789,4014813,4014909,4014991,4015119,4015155,4015234,4015260,4015444,4015497,4015514,4015516,4015550,4015589,4015638,4015639,4015772,4015853,4015900,4016007,4016047,4016112,4016127,4016139,4016187,4016284,4016293,4016311,4016323,4016367,4016452,8300015,8300017,8500149,8500222,10200002,10200012,10200029,10200030,10200040,10200048,10200053,10200055,10200066,10200083,10200103,10200120,10200134,10200155,10200157,10200169,10200177&ei=40I4U43cE5LUsAS23oHoBw&imc=1&imn=1&imp=1&atyp=csi&adh=&xjs=init.70.34.sb.41.m.7.p.6.actn.2.ifl.2&rt=xjsls.92,prt.94,xjses.1739,xjsee.1862,xjs.1876,ol.3856,iml.291,wsrt.708,cst.0,dnst.0,rqst.799,rspt.799
 4591.271 |  4905.102 |   313.831 | http://www.google.com/images/nav_logo170.png
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf. -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
          OK |       text/html |         | http://www.google.com/textinputassistant/tia.png -> nil
file not found |       text/html |         | http://www.google.com/gen_204?v=3&s=webhp&action=&e=4006,17259,4000116,4007661,4007830,4008067,4008133,4008142,4009033,4009352,4009565,4009641,4010806,4010858,4010899,4011228,4011258,4011679,4012373,4012504,4012508,4013374,4013414,4013591,4013723,4013747,4013787,4013823,4013967,4013979,4014016,4014092,4014431,4014515,4014637,4014649,4014671,4014789,4014813,4014909,4014991,4015119,4015155,4015234,4015260,4015444,4015497,4015514,4015516,4015550,4015589,4015638,4015639,4015772,4015853,4015900,4016007,4016047,4016112,4016127,4016139,4016187,4016284,4016293,4016311,4016323,4016367,4016452,8300015,8300017,8500149,8500222,10200002,10200012,10200029,10200030,10200040,10200048,10200053,10200055,10200066,10200083,10200103,10200120,10200134,10200155,10200157,10200169,10200177&ei=40I4U43cE5LUsAS23oHoBw&imc=1&imn=1&imp=1&atyp=csi&adh=&xjs=init.70.34.sb.41.m.7.p.6.actn.2.ifl.2&rt=xjsls.92,prt.94,xjses.1739,xjsee.1862,xjs.1876,ol.3856,iml.291,wsrt.708,cst.0,dnst.0,rqst.799,rspt.799 -> nil
          OK |       text/html |         | http://www.google.com/images/nav_logo170.png -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 10 samples, average = 124590.4 (flags = 0x1)
0       ------------------------------------------------------------------------O (2 = 20.0%)
500     ... 
12985   ------------------------------------O                                     (1 = 10.0%) {20.0%}
14032   O                                                                         (0 = 0.0%) {30.0%}
15164   ------------------------------------O                                     (1 = 10.0%) {30.0%}
16387   ... 
52461   ------------------------------------O                                     (1 = 10.0%) {40.0%}
56692   ... 
71547   ------------------------------------O                                     (1 = 10.0%) {50.0%}
77318   ... 
133075  ------------------------------------O                                     (1 = 10.0%) {60.0%}
143809  ------------------------------------O                                     (1 = 10.0%) {70.0%}
155408  ... 
364793  ------------------------------------O                                     (1 = 10.0%) {80.0%}
394217  ------------------------------------O                                     (1 = 10.0%) {90.0%}
426014  ... 

Histogram: Net.ConnectionTypeCount3 recorded 23 samples, average = 3.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 56.5%)
1  ... 
7  -------------------------------------------------------O                  (10 = 43.5%) {56.5%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 13 samples, average = 107.5 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (11 = 84.6%) {0.0%}
113  -------------O                                                            (2 = 15.4%) {84.6%}
128  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 10 samples, average = 105.6 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (10 = 100.0%) {0.0%}
113  ... 

Histogram: Net.HttpJob.TotalTime recorded 11 samples, average = 763.5 (flags = 0x1)
0     ... 
190   ------------------------------------------------O                         (2 = 18.2%) {0.0%}
226   O                                                                         (0 = 0.0%) {18.2%}
268   ------------------------O                                                 (1 = 9.1%) {18.2%}
318   O                                                                         (0 = 0.0%) {27.3%}
378   ------------------------O                                                 (1 = 9.1%) {27.3%}
449   ------------------------O                                                 (1 = 9.1%) {36.4%}
533   O                                                                         (0 = 0.0%) {45.5%}
633   ------------------------------------------------------------------------O (3 = 27.3%) {45.5%}
752   ... 
1062  ------------------------O                                                 (1 = 9.1%) {72.7%}
1262  O                                                                         (0 = 0.0%) {81.8%}
1500  ------------------------O                                                 (1 = 9.1%) {81.8%}
1782  ------------------------O                                                 (1 = 9.1%) {90.9%}
2117  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 697.0 (flags = 0x1)
0    ... 
633  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
752  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 10 samples, average = 770.2 (flags = 0x1)
0     ... 
190   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
226   O                                                                         (0 = 0.0%) {20.0%}
268   ------------------------------------O                                     (1 = 10.0%) {20.0%}
318   O                                                                         (0 = 0.0%) {30.0%}
378   ------------------------------------O                                     (1 = 10.0%) {30.0%}
449   ------------------------------------O                                     (1 = 10.0%) {40.0%}
533   O                                                                         (0 = 0.0%) {50.0%}
633   ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
752   ... 
1062  ------------------------------------O                                     (1 = 10.0%) {70.0%}
1262  O                                                                         (0 = 0.0%) {80.0%}
1500  ------------------------------------O                                     (1 = 10.0%) {80.0%}
1782  ------------------------------------O                                     (1 = 10.0%) {90.0%}
2117  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 10 samples, average = 770.2 (flags = 0x1)
0     ... 
190   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
226   O                                                                         (0 = 0.0%) {20.0%}
268   ------------------------------------O                                     (1 = 10.0%) {20.0%}
318   O                                                                         (0 = 0.0%) {30.0%}
378   ------------------------------------O                                     (1 = 10.0%) {30.0%}
449   ------------------------------------O                                     (1 = 10.0%) {40.0%}
533   O                                                                         (0 = 0.0%) {50.0%}
633   ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
752   ... 
1062  ------------------------------------O                                     (1 = 10.0%) {70.0%}
1262  O                                                                         (0 = 0.0%) {80.0%}
1500  ------------------------------------O                                     (1 = 10.0%) {80.0%}
1782  ------------------------------------O                                     (1 = 10.0%) {90.0%}
2117  ... 

Histogram: Net.HttpResponseCode recorded 10 samples, average = 220.4 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (9 = 90.0%) {0.0%}
201  ... 
404  ------------------------------------------------------------------------O (1 = 10.0%) {90.0%}
405  ... 

Histogram: Net.HttpSocketType recorded 10 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (10 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 11 samples, average = 400.0 (flags = 0x1)
0     ... 
171   ------------------------O                                                 (2 = 18.2%) {0.0%}
210   ------------------------------------------------------------------------O (6 = 54.5%) {18.2%}
258   ... 
477   ------------O                                                             (1 = 9.1%) {72.7%}
585   ------------O                                                             (1 = 9.1%) {81.8%}
718   ... 
1326  ------------O                                                             (1 = 9.1%) {90.9%}
1627  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 13 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (13 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 13 samples, average = 107.6 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (11 = 84.6%) {0.0%}
113  -------------O                                                            (2 = 15.4%) {84.6%}
128  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 13 samples, average = 107.5 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (11 = 84.6%) {0.0%}
113  -------------O                                                            (2 = 15.4%) {84.6%}
128  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 13 samples, average = 107.5 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (11 = 84.6%) {0.0%}
113  -------------O                                                            (2 = 15.4%) {84.6%}
128  ... 

Histogram: Net.Transaction_Latency_Total recorded 10 samples, average = 769.6 (flags = 0x1)
0     ... 
186   ------------------------------------O                                     (1 = 10.0%) {0.0%}
211   ------------------------------------O                                     (1 = 10.0%) {10.0%}
239   ... 
307   ------------------------------------O                                     (1 = 10.0%) {20.0%}
348   O                                                                         (0 = 0.0%) {30.0%}
394   ------------------------------------O                                     (1 = 10.0%) {30.0%}
446   O                                                                         (0 = 0.0%) {40.0%}
505   ------------------------------------O                                     (1 = 10.0%) {40.0%}
572   O                                                                         (0 = 0.0%) {50.0%}
648   ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
734   ... 
1065  ------------------------------------O                                     (1 = 10.0%) {70.0%}
1206  O                                                                         (0 = 0.0%) {80.0%}
1365  ------------------------------------O                                     (1 = 10.0%) {80.0%}
1546  ... 
1981  ------------------------------------O                                     (1 = 10.0%) {90.0%}
2243  ... 

Histogram: Net.Transaction_Latency_Total_New_Connection recorded 10 samples, average = 769.6 (flags = 0x1)
0     ... 
186   ------------------------------------O                                     (1 = 10.0%) {0.0%}
211   ------------------------------------O                                     (1 = 10.0%) {10.0%}
239   ... 
307   ------------------------------------O                                     (1 = 10.0%) {20.0%}
348   O                                                                         (0 = 0.0%) {30.0%}
394   ------------------------------------O                                     (1 = 10.0%) {30.0%}
446   O                                                                         (0 = 0.0%) {40.0%}
505   ------------------------------------O                                     (1 = 10.0%) {40.0%}
572   O                                                                         (0 = 0.0%) {50.0%}
648   ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
734   ... 
1065  ------------------------------------O                                     (1 = 10.0%) {70.0%}
1206  O                                                                         (0 = 0.0%) {80.0%}
1365  ------------------------------------O                                     (1 = 10.0%) {80.0%}
1546  ... 
1981  ------------------------------------O                                     (1 = 10.0%) {90.0%}
2243  ... 

Histogram: Net.Transaction_Latency_b recorded 10 samples, average = 663.3 (flags = 0x1)
0     ... 
100   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
113   ... 
186   ------------------------------------O                                     (1 = 10.0%) {20.0%}
211   ... 
307   ------------------------------------O                                     (1 = 10.0%) {30.0%}
348   O                                                                         (0 = 0.0%) {40.0%}
394   ------------------------------------O                                     (1 = 10.0%) {40.0%}
446   O                                                                         (0 = 0.0%) {50.0%}
505   ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
572   ... 
941   ------------------------------------O                                     (1 = 10.0%) {70.0%}
1065  ... 
1365  ------------------------------------O                                     (1 = 10.0%) {80.0%}
1546  O                                                                         (0 = 0.0%) {90.0%}
1750  ------------------------------------O                                     (1 = 10.0%) {90.0%}
1981  ... 


</queries>
