[24907:24912:8726360262:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[24907:24915:8730259573:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[24907:24915:8730467376:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[24907:24915:8730675866:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	1
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	6853
c:URLRequestCount:	11
c:disk_cache.miss:	11
c:HttpNetworkTransaction.Count:	11
c:tcp.connect:	13
c:tcp.write_bytes:	5710
c:tcp.read_bytes:	1248304
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  124.111 |  6474.325 |  6350.214 |   0 | www.google.com:80 ->  10.0.0.5:8000
  124.346 |   124.346 |     0.000 |   1 | www.google.com:80 ->  nil
 1614.628 |  1614.668 |     0.040 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
 1614.665 |  1614.665 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
 1640.431 |  1640.431 |     0.000 |   1 | www.google.com:80 ->  nil
 1640.455 |  1640.455 |     0.000 |   1 | www.google.com:80 ->  nil
 1677.950 |  1677.950 |     0.000 |   1 | www.google.com:80 ->  nil
 1677.976 |  1677.976 |     0.000 |   1 | www.google.com:80 ->  nil
 1864.511 |  1864.541 |     0.030 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
 1864.538 |  1864.538 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 3389.225 |  4220.733 |   831.508 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 3389.319 |  3389.319 |     0.000 |   1 | apis.google.com:443 ->  nil
 3389.325 |  3389.325 |     0.000 |   1 | apis.google.com:443 ->  nil
 3723.464 |  3723.464 |     0.000 |   1 | www.google.com:80 ->  nil
 3723.501 |  3723.501 |     0.000 |   1 | www.google.com:80 ->  nil
 3734.341 |  3734.341 |     0.000 |   1 | www.google.com:80 ->  nil
 3734.366 |  3734.366 |     0.000 |   1 | www.google.com:80 ->  nil
 3757.497 |  3757.497 |     0.000 |   1 | www.google.com:80 ->  nil
 3757.523 |  3757.523 |     0.000 |   1 | www.google.com:80 ->  nil
 4012.717 |  4012.717 |     0.000 |   1 | apis.google.com:443 ->  nil
 4012.761 |  4012.761 |     0.000 |   1 | apis.google.com:443 ->  nil
 4012.767 |  4012.767 |     0.000 |   1 | apis.google.com:443 ->  nil
 4220.632 |  4220.632 |     0.000 |   1 | apis.google.com:443 ->  nil
 4220.715 |  4220.715 |     0.000 |   1 | apis.google.com:443 ->  nil
 4220.729 |  4220.729 |     0.000 |   1 | apis.google.com:443 ->  nil
 6469.953 |  6469.953 |     0.000 |   1 | www.google.com:80 ->  nil
 6469.979 |  6469.979 |     0.000 |   1 | www.google.com:80 ->  nil
 6474.297 |  6474.297 |     0.000 |   1 | www.google.com:80 ->  nil
 6474.322 |  6474.322 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  123.948 |  1638.388 |  1514.440 | http://www.google.com/
 1640.412 |  1854.789 |   214.377 | http://www.google.com/images/srpr/logo11w.png
 1614.609 |  2637.283 |  1022.674 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
 1864.489 |  3330.395 |  1465.906 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 1677.929 |  3551.424 |  1873.495 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 3389.196 |  4430.000 |  1040.804 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
 3723.443 |  4539.382 |   815.939 | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf.
 3734.319 |  6277.707 |  2543.388 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 3757.476 |  6456.533 |  2699.057 | http://www.google.com/textinputassistant/tia.png
 6474.274 |  6688.022 |   213.748 | http://www.google.com/gen_204?v=3&s=webhp&action=&e=4006,17259,4000116,4007661,4007830,4008067,4008133,4008142,4009033,4009352,4009565,4009641,4010806,4010858,4010899,4011228,4011258,4011679,4012373,4012504,4012508,4013374,4013414,4013591,4013723,4013747,4013787,4013823,4013967,4013979,4014016,4014092,4014431,4014515,4014637,4014649,4014671,4014789,4014813,4014909,4014991,4015119,4015155,4015234,4015260,4015444,4015497,4015514,4015516,4015550,4015589,4015638,4015639,4015772,4015853,4015900,4016007,4016047,4016112,4016127,4016139,4016187,4016284,4016293,4016311,4016323,4016367,4016452,8300015,8300017,8500149,8500222,10200002,10200012,10200029,10200030,10200040,10200048,10200053,10200055,10200066,10200083,10200103,10200120,10200134,10200155,10200157,10200169,10200177&ei=40I4U43cE5LUsAS23oHoBw&imc=1&imn=1&imp=1&atyp=csi&adh=&xjs=init.66.34.sb.41.p.6.m.6.ifl.2.imap.2&rt=xjsls.76,prt.77,xjses.2113,xjsee.2222,xjs.2236,ol.4901,iml.294,wsrt.1538,cst.0,dnst.0,rqst.1608,rspt.1608
 6469.926 |  6884.289 |   414.363 | http://www.google.com/images/nav_logo170.png
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf. -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
          OK |       text/html |         | http://www.google.com/textinputassistant/tia.png -> nil
file not found |       text/html |         | http://www.google.com/gen_204?v=3&s=webhp&action=&e=4006,17259,4000116,4007661,4007830,4008067,4008133,4008142,4009033,4009352,4009565,4009641,4010806,4010858,4010899,4011228,4011258,4011679,4012373,4012504,4012508,4013374,4013414,4013591,4013723,4013747,4013787,4013823,4013967,4013979,4014016,4014092,4014431,4014515,4014637,4014649,4014671,4014789,4014813,4014909,4014991,4015119,4015155,4015234,4015260,4015444,4015497,4015514,4015516,4015550,4015589,4015638,4015639,4015772,4015853,4015900,4016007,4016047,4016112,4016127,4016139,4016187,4016284,4016293,4016311,4016323,4016367,4016452,8300015,8300017,8500149,8500222,10200002,10200012,10200029,10200030,10200040,10200048,10200053,10200055,10200066,10200083,10200103,10200120,10200134,10200155,10200157,10200169,10200177&ei=40I4U43cE5LUsAS23oHoBw&imc=1&imn=1&imp=1&atyp=csi&adh=&xjs=init.66.34.sb.41.p.6.m.6.ifl.2.imap.2&rt=xjsls.76,prt.77,xjses.2113,xjsee.2222,xjs.2236,ol.4901,iml.294,wsrt.1538,cst.0,dnst.0,rqst.1608,rspt.1608 -> nil
          OK |       text/html |         | http://www.google.com/images/nav_logo170.png -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 10 samples, average = 124590.4 (flags = 0x1)
0       ------------------------------------------------------------------------O (2 = 20.0%)
500     ... 
12985   ------------------------------------O                                     (1 = 10.0%) {20.0%}
14032   O                                                                         (0 = 0.0%) {30.0%}
15164   ------------------------------------O                                     (1 = 10.0%) {30.0%}
16387   ... 
52461   ------------------------------------O                                     (1 = 10.0%) {40.0%}
56692   ... 
71547   ------------------------------------O                                     (1 = 10.0%) {50.0%}
77318   ... 
133075  ------------------------------------O                                     (1 = 10.0%) {60.0%}
143809  ------------------------------------O                                     (1 = 10.0%) {70.0%}
155408  ... 
364793  ------------------------------------O                                     (1 = 10.0%) {80.0%}
394217  ------------------------------------O                                     (1 = 10.0%) {90.0%}
426014  ... 

Histogram: Net.ConnectionTypeCount3 recorded 23 samples, average = 3.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 56.5%)
1  ... 
7  -------------------------------------------------------O                  (10 = 43.5%) {56.5%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 10 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (10 = 100.0%)
1  ... 

Histogram: Net.HttpJob.TotalTime recorded 11 samples, average = 1255.2 (flags = 0x1)
0     ... 
190   ------------------------------------------------------------------------O (2 = 18.2%) {0.0%}
226   ... 
378   ------------------------------------O                                     (1 = 9.1%) {18.2%}
449   ... 
752   ------------------------------------O                                     (1 = 9.1%) {27.3%}
894   ------------------------------------------------------------------------O (2 = 18.2%) {36.4%}
1062  O                                                                         (0 = 0.0%) {54.5%}
1262  ------------------------------------O                                     (1 = 9.1%) {54.5%}
1500  ------------------------------------O                                     (1 = 9.1%) {63.6%}
1782  ------------------------------------O                                     (1 = 9.1%) {72.7%}
2117  O                                                                         (0 = 0.0%) {81.8%}
2516  ------------------------------------------------------------------------O (2 = 18.2%) {81.8%}
2990  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 1043.0 (flags = 0x1)
0    ... 
894  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
1062 ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 10 samples, average = 1276.4 (flags = 0x1)
0     ... 
190   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
226   ... 
378   ------------------------------------O                                     (1 = 10.0%) {20.0%}
449   ... 
752   ------------------------------------O                                     (1 = 10.0%) {30.0%}
894   ------------------------------------O                                     (1 = 10.0%) {40.0%}
1062  O                                                                         (0 = 0.0%) {50.0%}
1262  ------------------------------------O                                     (1 = 10.0%) {50.0%}
1500  ------------------------------------O                                     (1 = 10.0%) {60.0%}
1782  ------------------------------------O                                     (1 = 10.0%) {70.0%}
2117  O                                                                         (0 = 0.0%) {80.0%}
2516  ------------------------------------------------------------------------O (2 = 20.0%) {80.0%}
2990  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 10 samples, average = 1276.4 (flags = 0x1)
0     ... 
190   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
226   ... 
378   ------------------------------------O                                     (1 = 10.0%) {20.0%}
449   ... 
752   ------------------------------------O                                     (1 = 10.0%) {30.0%}
894   ------------------------------------O                                     (1 = 10.0%) {40.0%}
1062  O                                                                         (0 = 0.0%) {50.0%}
1262  ------------------------------------O                                     (1 = 10.0%) {50.0%}
1500  ------------------------------------O                                     (1 = 10.0%) {60.0%}
1782  ------------------------------------O                                     (1 = 10.0%) {70.0%}
2117  O                                                                         (0 = 0.0%) {80.0%}
2516  ------------------------------------------------------------------------O (2 = 20.0%) {80.0%}
2990  ... 

Histogram: Net.HttpResponseCode recorded 10 samples, average = 220.4 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (9 = 90.0%) {0.0%}
201  ... 
404  ------------------------------------------------------------------------O (1 = 10.0%) {90.0%}
405  ... 

Histogram: Net.HttpSocketType recorded 10 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (10 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 11 samples, average = 660.3 (flags = 0x1)
0     ... 
171   ------------------------------------------------------------------------O (3 = 27.3%) {0.0%}
210   ------------------------------------------------O                         (2 = 18.2%) {27.3%}
258   ... 
585   ------------------------------------------------------------------------O (3 = 27.3%) {45.5%}
718   ------------------------O                                                 (1 = 9.1%) {72.7%}
881   ------------------------O                                                 (1 = 9.1%) {81.8%}
1081  ... 
2449  ------------------------O                                                 (1 = 9.1%) {90.9%}
3005  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 13 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (13 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.Transaction_Latency_Total recorded 10 samples, average = 1276.0 (flags = 0x1)
0     ... 
211   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
239   ... 
394   ------------------------------------O                                     (1 = 10.0%) {20.0%}
446   ... 
734   ------------------------------------O                                     (1 = 10.0%) {30.0%}
831   O                                                                         (0 = 0.0%) {40.0%}
941   ------------------------------------O                                     (1 = 10.0%) {40.0%}
1065  ... 
1365  ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
1546  O                                                                         (0 = 0.0%) {70.0%}
1750  ------------------------------------O                                     (1 = 10.0%) {70.0%}
1981  O                                                                         (0 = 0.0%) {80.0%}
2243  ------------------------------------O                                     (1 = 10.0%) {80.0%}
2540  ------------------------------------O                                     (1 = 10.0%) {90.0%}
2876  ... 

Histogram: Net.Transaction_Latency_Total_New_Connection recorded 10 samples, average = 1276.0 (flags = 0x1)
0     ... 
211   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
239   ... 
394   ------------------------------------O                                     (1 = 10.0%) {20.0%}
446   ... 
734   ------------------------------------O                                     (1 = 10.0%) {30.0%}
831   O                                                                         (0 = 0.0%) {40.0%}
941   ------------------------------------O                                     (1 = 10.0%) {40.0%}
1065  ... 
1365  ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
1546  O                                                                         (0 = 0.0%) {70.0%}
1750  ------------------------------------O                                     (1 = 10.0%) {70.0%}
1981  O                                                                         (0 = 0.0%) {80.0%}
2243  ------------------------------------O                                     (1 = 10.0%) {80.0%}
2540  ------------------------------------O                                     (1 = 10.0%) {90.0%}
2876  ... 

Histogram: Net.Transaction_Latency_b recorded 10 samples, average = 1275.9 (flags = 0x1)
0     ... 
211   ------------------------------------------------------------------------O (2 = 20.0%) {0.0%}
239   ... 
394   ------------------------------------O                                     (1 = 10.0%) {20.0%}
446   ... 
734   ------------------------------------O                                     (1 = 10.0%) {30.0%}
831   O                                                                         (0 = 0.0%) {40.0%}
941   ------------------------------------O                                     (1 = 10.0%) {40.0%}
1065  ... 
1365  ------------------------------------------------------------------------O (2 = 20.0%) {50.0%}
1546  O                                                                         (0 = 0.0%) {70.0%}
1750  ------------------------------------O                                     (1 = 10.0%) {70.0%}
1981  O                                                                         (0 = 0.0%) {80.0%}
2243  ------------------------------------O                                     (1 = 10.0%) {80.0%}
2540  ------------------------------------O                                     (1 = 10.0%) {90.0%}
2876  ... 


</queries>
