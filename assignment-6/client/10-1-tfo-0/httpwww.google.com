[21067:21072:8561078353:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[21067:21075:8562113038:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[21067:21075:8562168721:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[21067:21075:8562218299:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	0
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	1297
c:URLRequestCount:	9
c:disk_cache.miss:	9
c:HttpNetworkTransaction.Count:	9
c:tcp.connect:	11
c:tcp.write_bytes:	4090
c:tcp.read_bytes:	840898
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  149.381 |  1533.836 |  1384.455 |   0 | www.google.com:80 ->  10.0.0.5:8000
  149.701 |   149.701 |     0.000 |   1 | www.google.com:80 ->  nil
  462.448 |   462.448 |     0.000 |   1 | www.google.com:80 ->  nil
  462.482 |   462.482 |     0.000 |   1 | www.google.com:80 ->  nil
  468.635 |   468.665 |     0.030 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
  468.663 |   468.663 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
  508.548 |   508.548 |     0.000 |   1 | www.google.com:80 ->  nil
  508.573 |   508.573 |     0.000 |   1 | www.google.com:80 ->  nil
  731.073 |   731.141 |     0.068 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
  731.134 |   731.134 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 1124.976 |  1229.522 |   104.546 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 1125.069 |  1125.069 |     0.000 |   1 | apis.google.com:443 ->  nil
 1125.074 |  1125.074 |     0.000 |   1 | apis.google.com:443 ->  nil
 1173.831 |  1173.831 |     0.000 |   1 | apis.google.com:443 ->  nil
 1173.871 |  1173.871 |     0.000 |   1 | apis.google.com:443 ->  nil
 1173.876 |  1173.876 |     0.000 |   1 | apis.google.com:443 ->  nil
 1229.471 |  1229.471 |     0.000 |   1 | apis.google.com:443 ->  nil
 1229.515 |  1229.515 |     0.000 |   1 | apis.google.com:443 ->  nil
 1229.521 |  1229.521 |     0.000 |   1 | apis.google.com:443 ->  nil
 1498.287 |  1498.287 |     0.000 |   1 | www.google.com:80 ->  nil
 1498.314 |  1498.314 |     0.000 |   1 | www.google.com:80 ->  nil
 1509.015 |  1509.015 |     0.000 |   1 | www.google.com:80 ->  nil
 1509.041 |  1509.041 |     0.000 |   1 | www.google.com:80 ->  nil
 1533.811 |  1533.811 |     0.000 |   1 | www.google.com:80 ->  nil
 1533.833 |  1533.833 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  149.203 |   503.278 |   354.075 | http://www.google.com/
  462.427 |   550.188 |    87.761 | http://www.google.com/images/srpr/logo11w.png
  468.598 |   597.538 |   128.940 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
  731.026 |  1061.690 |   330.664 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 1124.945 |  1281.501 |   156.556 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
  508.527 |  1332.972 |   824.445 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 1498.267 |  1723.700 |   225.433 | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf.
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
          OK |       text/html |         | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf. -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 6 samples, average = 135784.2 (flags = 0x1)
0       ... 
12985   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
14032   ... 
52461   ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
56692   ... 
71547   ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
77318   ... 
133075  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
143809  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
155408  ... 
364793  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
394217  ... 

Histogram: Net.ConnectionTypeCount3 recorded 19 samples, average = 2.9 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 57.9%)
1  ... 
7  ----------------------------------------------------O                     (8 = 42.1%) {57.9%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 11 samples, average = 25.0 (flags = 0x1)
0   ... 
23  ------------------------------------------------------------------------O (9 = 81.8%) {0.0%}
26  --------O                                                                 (1 = 9.1%) {81.8%}
29  ------O                                                                   (1 = 9.1%) {90.9%}
33  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 8 samples, average = 24.5 (flags = 0x1)
0   ... 
23  ------------------------------------------------------------------------O (7 = 87.5%) {0.0%}
26  ----------O                                                               (1 = 12.5%) {87.5%}
29  ... 

Histogram: Net.HttpJob.TotalTime recorded 7 samples, average = 300.9 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 14.3%) {0.0%}
96   O                                                                         (0 = 0.0%) {14.3%}
114  ------------------------------------O                                     (1 = 14.3%) {14.3%}
135  O                                                                         (0 = 0.0%) {28.6%}
160  ------------------------------------O                                     (1 = 14.3%) {28.6%}
190  ------------------------------------O                                     (1 = 14.3%) {42.9%}
226  ... 
318  ------------------------------------------------------------------------O (2 = 28.6%) {57.1%}
378  ... 
752  ------------------------------------O                                     (1 = 14.3%) {85.7%}
894  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 167.0 (flags = 0x1)
0    ... 
160  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
190  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 6 samples, average = 323.2 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 16.7%) {0.0%}
96   O                                                                         (0 = 0.0%) {16.7%}
114  ------------------------------------O                                     (1 = 16.7%) {16.7%}
135  ... 
190  ------------------------------------O                                     (1 = 16.7%) {33.3%}
226  ... 
318  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
378  ... 
752  ------------------------------------O                                     (1 = 16.7%) {83.3%}
894  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 6 samples, average = 323.2 (flags = 0x1)
0    ... 
81   ------------------------------------O                                     (1 = 16.7%) {0.0%}
96   O                                                                         (0 = 0.0%) {16.7%}
114  ------------------------------------O                                     (1 = 16.7%) {16.7%}
135  ... 
190  ------------------------------------O                                     (1 = 16.7%) {33.3%}
226  ... 
318  ------------------------------------------------------------------------O (2 = 33.3%) {50.0%}
378  ... 
752  ------------------------------------O                                     (1 = 16.7%) {83.3%}
894  ... 

Histogram: Net.HttpResponseCode recorded 7 samples, average = 200.0 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (7 = 100.0%) {0.0%}
201  ... 

Histogram: Net.HttpSocketType recorded 8 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (8 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 8 samples, average = 77.1 (flags = 0x1)
0    ... 
41   ------------------------------------------------------------------------O (3 = 37.5%) {0.0%}
50   ------------------------------------------------------------------------O (3 = 37.5%) {37.5%}
61   ... 
139  ------------------------------------------------O                         (2 = 25.0%) {75.0%}
171  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 9 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (9 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 11 samples, average = 25.1 (flags = 0x1)
0   ... 
23  ------------------------------------------------------------------------O (9 = 81.8%) {0.0%}
26  --------O                                                                 (1 = 9.1%) {81.8%}
29  ------O                                                                   (1 = 9.1%) {90.9%}
33  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 11 samples, average = 25.0 (flags = 0x1)
0   ... 
23  ------------------------------------------------------------------------O (9 = 81.8%) {0.0%}
26  --------O                                                                 (1 = 9.1%) {81.8%}
29  ------O                                                      [21067:21072:8562669544:FATAL:url_request_context.cc(121)] Check failed: false. Leaked 2 URLRequest(s). First URL: http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw.
             (1 = 9.1%) {90.9%}
33  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 11 samples, average = 25.0 (flags = 0x1)
0   ... 
23  ------------------------------------------------------------------------O (9 = 81.8%) {0.0%}
26  --------O                                                                 (1 = 9.1%) {81.8%}
29  ------O                                                                   (1 = 9.1%) {90.9%}
33  ... 

Histogram: Net.Transaction_Latency_Total recorded 6 samples, average = 322.3 (flags = 0x1)
0    ... 
78   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
88   ... 
128  ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
145  ... 
211  ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
239  ... 
307  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
348  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
394  ... 
734  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
831  ... 

Histogram: Net.Transaction_Latency_Total_New_Connection recorded 6 samples, average = 322.3 (flags = 0x1)
0    ... 
78   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
88   ... 
128  ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
145  ... 
211  ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
239  ... 
307  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
348  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
394  ... 
734  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
831  ... 

Histogram: Net.Transaction_Latency_b recorded 6 samples, average = 297.2 (flags = 0x1)
0    ... 
61   ------------------------------------------------------------------------O (1 = 16.7%) {0.0%}
69   ... 
100  ------------------------------------------------------------------------O (1 = 16.7%) {16.7%}
113  ... 
186  ------------------------------------------------------------------------O (1 = 16.7%) {33.3%}
211  ... 
271  ------------------------------------------------------------------------O (1 = 16.7%) {50.0%}
307  ------------------------------------------------------------------------O (1 = 16.7%) {66.7%}
348  ... 
734  ------------------------------------------------------------------------O (1 = 16.7%) {83.3%}
831  ... 


</queries>
Received signal 6
 [0x000000531f1e] base::debug::StackTrace::StackTrace()
 [0x000000532418] base::debug::(anonymous namespace)::StackDumpSignalHandler()
 [0x7fca190adbd0] <unknown>
 [0x7fca180d7037] gsignal
 [0x7fca180da698] abort
 [0x00000056c349] base::debug::BreakDebugger()
 [0x0000005405cd] logging::LogMessage::~LogMessage()
 [0x000001c2eb7e] net::URLRequestContext::AssertNoURLRequests()
 [0x000001c2ebc8] net::URLRequestContext::~URLRequestContext()
 [0x000001aec899] TestShellRequestContext::~TestShellRequestContext()
 [0x000001ae3c1f] (anonymous namespace)::IOThread::CleanUp()
 [0x00000055eafd] base::Thread::ThreadMain()
 [0x00000055e361] base::(anonymous namespace)::ThreadFunc()
 [0x7fca190a5f8e] start_thread
 [0x7fca18199e1d] clone
  r8: 000000000204024d  r9: 00007fca11903b26 r10: 0000000000000008 r11: 0000000000000206
 r12: 0000000000000000 r13: 00007fca18462848 r14: 00007fffa4bc63a0 r15: 0000000000001000
  di: 000000000000524b  si: 0000000000005250  bp: 00007fca18c854c0  bx: 00007fca11904620
  dx: 0000000000000006  ax: 0000000000000000  cx: ffffffffffffffff  sp: 00007fca11903dc8
  ip: 00007fca180d7037 efl: 0000000000000206 cgf: 0000000000000033 erf: 0000000000000000
 trp: 0000000000000000 msk: 0000000000000000 cr2: 0000000000000000
