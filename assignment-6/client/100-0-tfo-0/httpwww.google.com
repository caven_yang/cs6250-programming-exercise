[22584:22589:8618240249:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[22584:22592:8622135320:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[22584:22592:8640432608:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[22584:22592:8640841254:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	0
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	22710
c:URLRequestCount:	11
c:disk_cache.miss:	11
c:HttpNetworkTransaction.Count:	11
c:tcp.connect:	16
c:socket.backup_created:	4
c:tcp.write_bytes:	4090
c:tcp.read_bytes:	1231310
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  152.088 | 22744.156 | 22592.068 |   0 | www.google.com:80 ->  10.0.0.5:8000
  152.375 |   152.375 |     0.000 |   1 | www.google.com:80 ->  nil
  403.436 |   403.436 |     0.000 |   1 | www.google.com:80 ->  nil
 1658.770 |  1909.334 |   250.564 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
 1658.801 |  1658.801 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
 1667.736 |  1667.736 |     0.000 |   1 | www.google.com:80 ->  nil
 1683.272 |  1683.272 |     0.000 |   1 | www.google.com:80 ->  nil
 1683.298 |  1683.298 |     0.000 |   1 | www.google.com:80 ->  nil
 1899.830 |  2150.704 |   250.874 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
 1899.861 |  1899.861 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 1909.317 |  1909.317 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
 2150.684 |  2150.684 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 3407.714 | 22329.742 | 18922.028 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 3407.789 |  3407.789 |     0.000 |   1 | apis.google.com:443 ->  nil
 3407.795 |  3407.795 |     0.000 |   1 | apis.google.com:443 ->  nil
 3659.098 |  3659.098 |     0.000 |   1 | apis.google.com:443 ->  nil
 3659.126 |  3659.126 |     0.000 |   1 | apis.google.com:443 ->  nil
 3924.198 |  3924.198 |     0.000 |   1 | www.google.com:80 ->  nil
 3924.224 |  3924.224 |     0.000 |   1 | www.google.com:80 ->  nil
 3935.026 |  3935.026 |     0.000 |   1 | www.google.com:80 ->  nil
 3935.051 |  3935.051 |     0.000 |   1 | www.google.com:80 ->  nil
 3956.894 |  3956.894 |     0.000 |   1 | www.google.com:80 ->  nil
 3956.920 |  3956.920 |     0.000 |   1 | www.google.com:80 ->  nil
 4032.134 |  4032.134 |     0.000 |   1 | apis.google.com:443 ->  nil
 4032.180 |  4032.180 |     0.000 |   1 | apis.google.com:443 ->  nil
 4032.185 |  4032.185 |     0.000 |   1 | apis.google.com:443 ->  nil
22329.637 | 22329.637 |     0.000 |   1 | apis.google.com:443 ->  nil
22329.725 | 22329.725 |     0.000 |   1 | apis.google.com:443 ->  nil
22329.738 | 22329.738 |     0.000 |   1 | apis.google.com:443 ->  nil
22739.834 | 22739.834 |     0.000 |   1 | www.google.com:80 ->  nil
22739.860 | 22739.860 |     0.000 |   1 | www.google.com:80 ->  nil
22744.128 | 22744.128 |     0.000 |   1 | www.google.com:80 ->  nil
22744.153 | 22744.153 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  151.915 |  1661.223 |  1509.308 | http://www.google.com/
 1667.698 |  1875.164 |   207.466 | http://www.google.com/images/srpr/logo11w.png
 1658.747 |  2677.963 |  1019.216 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
 1899.807 |  3357.256 |  1457.449 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 1683.251 |  3749.930 |  2066.679 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 3924.177 |  4945.026 |  1020.849 | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf.
 3934.988 |  6501.544 |  2566.556 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 3956.874 |  6677.605 |  2720.731 | http://www.google.com/textinputassistant/tia.png
 3407.692 | 22738.051 | 19330.359 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
          OK |       text/html |         | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf. -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
          OK |       text/html |         | http://www.google.com/textinputassistant/tia.png -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 16 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (16 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 8 samples, average = 153671.8 (flags = 0x1)
0       ------------------------------------------------------------------------O (1 = 12.5%)
500     ... 
12985   ------------------------------------------------------------------------O (1 = 12.5%) {12.5%}
14032   ... 
52461   ------------------------------------------------------------------------O (1 = 12.5%) {25.0%}
56692   ... 
71547   ------------------------------------------------------------------------O (1 = 12.5%) {37.5%}
77318   ... 
133075  ------------------------------------------------------------------------O (1 = 12.5%) {50.0%}
143809  ------------------------------------------------------------------------O (1 = 12.5%) {62.5%}
155408  ... 
364793  ------------------------------------------------------------------------O (1 = 12.5%) {75.0%}
394217  ------------------------------------------------------------------------O (1 = 12.5%) {87.5%}
426014  ... 

Histogram: Net.ConnectionTypeCount3 recorded 22 samples, average = 2.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (14 = 63.6%)
1  ... 
7  -----------------------------------------O                                (8 = 36.4%) {63.6%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 14 samples, average = 263.4 (flags = 0x1)
0    ... 
186  ------------------------------------------------------------------------O (10 = 71.4%) {0.0%}
211  ... 
394  -----------------------------O                                            (4 = 28.6%) {71.4%}
446  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 7 samples, average = 292.9 (flags = 0x1)
0    ... 
186  ------------------------------------------------------------------------O (4 = 57.1%) {0.0%}
211  ... 
394  ------------------------------------------------------O                   (3 = 42.9%) {57.1%}
446  ... 

Histogram: Net.HttpJob.TotalTime recorded 9 samples, average = 3542.6 (flags = 0x1)
0      ... 
190    ------------------------------------O                                     (1 = 11.1%) {0.0%}
226    ... 
894    ------------------------------------------------------------------------O (2 = 22.2%) {11.1%}
1062   O                                                                         (0 = 0.0%) {33.3%}
1262   ------------------------------------O                                     (1 = 11.1%) {33.3%}
1500   ------------------------------------O                                     (1 = 11.1%) {44.4%}
1782   ------------------------------------O                                     (1 = 11.1%) {55.6%}
2117   O                                                                         (0 = 0.0%) {66.7%}
2516   ------------------------------------------------------------------------O (2 = 22.2%) {66.7%}
2990   ... 
10000  ------------------------------------O                                     (1 = 11.1%) {88.9%}

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 19330.0 (flags = 0x1)
0      ... 
10000  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}

Histogram: Net.HttpJob.TotalTimeNotCached recorded 8 samples, average = 1569.1 (flags = 0x1)
0     ... 
190   ------------------------------------O                                     (1 = 12.5%) {0.0%}
226   ... 
894   ------------------------------------------------------------------------O (2 = 25.0%) {12.5%}
1062  O                                                                         (0 = 0.0%) {37.5%}
1262  ------------------------------------O                                     (1 = 12.5%) {37.5%}
1500  ------------------------------------O                                     (1 = 12.5%) {50.0%}
1782  ------------------------------------O                                     (1 = 12.5%) {62.5%}
2117  O                                                                         (0 = 0.0%) {75.0%}
2516  ------------------------------------------------------------------------O (2 = 25.0%) {75.0%}
2990  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 8 samples, average = 1569.1 (flags = 0x1)
0     ... 
190   ------------------------------------O                                     (1 = 12.5%) {0.0%}
226   ... 
894   ------------------------------------------------------------------------O (2 = 25.0%) {12.5%}
1062  O                                                                         (0 = 0.0%) {37.5%}
1262  ------------------------------------O                                     (1 = 12.5%) {37.5%}
1500  ------------------------------------O                                     (1 = 12.5%) {50.0%}
1782  ------------------------------------O                                     (1 = 12.5%) {62.5%}
2117  O                                                                         (0 = 0.0%) {75.0%}
2516  ------------------------------------------------------------------------O (2 = 25.0%) {75.0%}
2990  ... 

Histogram: Net.HttpResponseCode recorded 8 samples, average = 200.0 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (8 = 100.0%) {0.0%}
201  ... 

Histogram: Net.HttpSocketType recorded 8 samples, average = 0.1 (flags = 0x1)
0  ------------------------------------------------------------------------O (7 = 87.5%)
1  ----------O                                                               (1 = 12.5%) {87.5%}
2  ... 

Histogram: Net.HttpTimeToFirstByte recorded 9 samples, average = 2847.1 (flags = 0x1)
0      ... 
171    ------------------------O                                                 (1 = 11.1%) {0.0%}
210    ... 
389    ------------------------------------------------O                         (2 = 22.2%) {11.1%}
477    O                                                                         (0 = 0.0%) {33.3%}
585    ------------------------------------------------------------------------O (3 = 33.3%) {33.3%}
718    ------------------------O                                                 (1 = 11.1%) {66.7%}
881    ... 
2449   ------------------------O                                                 (1 = 11.1%) {77.8%}
3005   ... 
18951  ------------------------O                                                 (1 = 11.1%) {88.9%}
23255  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 14 samples, average = 1.8 (flags = 0x1)
0  O                                                                         (0 = 0.0%)
1  --------------------O                                                     (3 = 21.4%) {0.0%}
2  ------------------------------------------------------------------------O (11 = 78.6%) {21.4%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket recorded 1 samples, average = 1059.0 (flags = 0x1)
0    ... 
997  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
1122 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 1 samples, average = 1059.0 (flags = 0x1)
0    ... 
997  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
1122 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 11 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (11 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 10 samples, average = 287.0 (flags = 0x1)
0    ... 
186  ------------------------------------------------------------------------O (6 = 60.0%) {0.0%}
211  ... 
394  ------------------------------------------------O                         (4 = 40.0%) {60.0%}
446  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 11 samples, average = 0.1 (flags = 0x1)
0  ------------------------------------------------------------------------O (10 = 90.9%)
1  -------O                                                                  (1 = 9.1%) {90.9%}
2  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 14 samples, average = 263.4 (flags = 0x1)
0    ... 
186  ------------------------------------------------------------------------O (10 = 71.4%) {0.0%}
211  ... 
394  -----------------------------O                                            (4 = 28.6%) {71.4%}
446  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 14 samples, average = 263.4 (flags = 0x1)
0    ... 
186  ------------------------------------------------------------------------O (10 = 71.4%) {0.0%}
211  ... 
394  -----------------------------O                                            (4 = 28.6%) {71.4%}
446  ... 

Histogram: Net.Transaction_Latency_Total [22584:22589:8640849140:FATAL:url_request_context.cc(121)] Check failed: false. Leaked 2 URLRequest(s). First URL: http://www.google.com/gen_204?v=3&s=webhp&action=&e=4006,17259,4000116,4007661,4007830,4008067,4008133,4008142,4009033,4009352,4009565,4009641,4010806,4010858,4010899,4011228,4011258,4011679,4012373,4012504,4012508,4013374,4013414,4013591,4013723,4013747,4013787,4013823,4013967,4013979,4014016,4014092,4014431,4014515,4014637,4014649,4014671,4014789,4014813,4014909,4014991,4015119,4015155,4015234,4015260,4015444,4015497,4015514,4015516,4015550,4015589,4015638,4015639,4015772,4015853,4015900,4016007,4016047,4016112,4016127,4016139,4016187,4016284,4016293,4016311,4016323,4016367,4016452,8300015,8300017,8500149,8500222,10200002,10200012,10200029,10200030,10200040,10200048,10200053,10200055,10200066,10200083,10200103,10200120,10200134,10200155,10200157,10200169,10200177&ei=40I4U43cE5LUsAS23oHoBw&imc=1&imn=1&imp=1&atyp=csi&adh=&xjs=init.65.34.sb.39.p.6.m.6.actn.2.imap.2&rt=xjsls.74,prt.75,xjses.2285,xjsee.2392,xjs.2417,ol.21142,iml.286,wsrt.1570,cst.0,dnst.0,rqst.1635,rspt.1635.
recorded 8 samples, average = 1568.6 (flags = 0x1)
0     ... 
186   ------------------------------------O                                     (1 = 12.5%) {0.0%}
211   ... 
941   ------------------------------------------------------------------------O (2 = 25.0%) {12.5%}
1065  ... 
1365  ------------------------------------------------------------------------O (2 = 25.0%) {37.5%}
1546  ... 
1981  ------------------------------------O                                     (1 = 12.5%) {62.5%}
2243  O                                                                         (0 = 0.0%) {75.0%}
2540  ------------------------------------------------------------------------O (2 = 25.0%) {75.0%}
2876  ... 

Histogram: Net.Transaction_Latency_Total_New_Connection recorded 7 samples, average = 1763.1 (flags = 0x1)
0     ... 
941   ------------------------------------------------------------------------O (2 = 28.6%) {0.0%}
1065  ... 
1365  ------------------------------------------------------------------------O (2 = 28.6%) {28.6%}
1546  ... 
1981  ------------------------------------O                                     (1 = 14.3%) {57.1%}
2243  O                                                                         (0 = 0.0%) {71.4%}
2540  ------------------------------------------------------------------------O (2 = 28.6%) {71.4%}
2876  ... 

Histogram: Net.Transaction_Latency_b recorded 8 samples, average = 1311.5 (flags = 0x1)
0     ... 
186   ------------------------------------O                                     (1 = 12.5%) {0.0%}
211   ... 
572   ------------------------------------O                                     (1 = 12.5%) {12.5%}
648   O                                                                         (0 = 0.0%) {25.0%}
734   ------------------------------------O                                     (1 = 12.5%) {25.0%}
831   O                                                                         (0 = 0.0%) {37.5%}
941   ------------------------------------O                                     (1 = 12.5%) {37.5%}
1065  ------------------------------------O                                     (1 = 12.5%) {50.0%}
1206  ... 
1750  ------------------------------------O                                     (1 = 12.5%) {62.5%}
1981  O                                                                         (0 = 0.0%) {75.0%}
2243  ------------------------------------------------------------------------O (2 = 25.0%) {75.0%}
2540  ... 


</queries>
Received signal 6
 [0x000000531f1e] base::debug::StackTrace::StackTrace()
 [0x000000532418] base::debug::(anonymous namespace)::StackDumpSignalHandler()
 [0x7f2d030dcbd0] <unknown>
 [0x7f2d02106037] gsignal
 [0x7f2d02109698] abort
 [0x00000056c349] base::debug::BreakDebugger()
 [0x0000005405cd] logging::LogMessage::~LogMessage()
 [0x000001c2eb7e] net::URLRequestContext::AssertNoURLRequests()
 [0x000001c2ebc8] net::URLRequestContext::~URLRequestContext()
 [0x000001aec899] TestShellRequestContext::~TestShellRequestContext()
 [0x000001ae3c1f] (anonymous namespace)::IOThread::CleanUp()
 [0x00000055eafd] base::Thread::ThreadMain()
 [0x00000055e361] base::(anonymous namespace)::ThreadFunc()
 [0x7f2d030d4f8e] start_thread
 [0x7f2d021c8e1d] clone
  r8: 000000000204024d  r9: 00007f2cfb932b26 r10: 0000000000000008 r11: 0000000000000202
 r12: 0000000000000000 r13: 00007f2d02491848 r14: 00007fff817157e0 r15: 0000000000001000
  di: 0000000000005838  si: 000000000000583d  bp: 00007f2d02cb44c0  bx: 00007f2cfb933620
  dx: 0000000000000006  ax: 0000000000000000  cx: ffffffffffffffff  sp: 00007f2cfb932dc8
  ip: 00007f2d02106037 efl: 0000000000000202 cgf: 0000000000000033 erf: 0000000000000000
 trp: 0000000000000000 msk: 0000000000000000 cr2: 0000000000000000
