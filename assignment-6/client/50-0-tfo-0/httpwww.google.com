[21812:21817:8583450722:WARNING:proxy_service.cc(889)] PAC support disabled because there is no system implementation
Loading hacky DNS from '/home/mininet/gt-cs6250/assignment-6/hack_dns' succeeded.
Remapping 'apis.google.com' -> '10.0.0.1' port 8000
Remapping 'suggestion.baidu.com' -> '10.0.0.2' port 8000
Remapping 'www.baidu.com' -> '10.0.0.3' port 8000
Remapping 's1.bdstatic.com' -> '10.0.0.4' port 8000
Remapping 'www.google.com' -> '10.0.0.5' port 8000
Remapping 'ssl.gstatic.com' -> '10.0.0.6' port 8000
Remapping 'passport.baidu.com' -> '10.0.0.7' port 8000
Remapping 'www.gstatic.com' -> '10.0.0.8' port 8000
[21812:21820:8585700281:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[21812:21820:8585935066:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
[21812:21820:8586146354:WARNING:nss_ssl_util.cc(230)] Unknown SSL error -12263 mapped to net::ERR_SSL_PROTOCOL_ERROR
<stats>
c:tfo.supported:	0
c:WebFrameActiveCount:	1
t:tfo.page_load_timer:	4261
c:URLRequestCount:	11
c:disk_cache.miss:	11
c:HttpNetworkTransaction.Count:	11
c:tcp.connect:	13
c:tcp.write_bytes:	5709
c:tcp.read_bytes:	1231516
</stats>

<resolves>
strt (ms) | end (ms)  | len (ms)  | err | url:port -> address_list
  162.404 |  4805.490 |  4643.086 |   0 | www.google.com:80 ->  10.0.0.5:8000
  162.644 |   162.644 |     0.000 |   1 | www.google.com:80 ->  nil
  946.853 |   946.891 |     0.038 |   0 | ssl.gstatic.com:80 ->  10.0.0.6:8000
  946.888 |   946.888 |     0.000 |   1 | ssl.gstatic.com:80 ->  nil
  972.360 |   972.360 |     0.000 |   1 | www.google.com:80 ->  nil
  972.392 |   972.392 |     0.000 |   1 | www.google.com:80 ->  nil
 1010.169 |  1010.169 |     0.000 |   1 | www.google.com:80 ->  nil
 1010.195 |  1010.195 |     0.000 |   1 | www.google.com:80 ->  nil
 1194.657 |  1194.686 |     0.029 |   0 | www.gstatic.com:80 ->  10.0.0.8:8000
 1194.683 |  1194.683 |     0.000 |   1 | www.gstatic.com:80 ->  nil
 2047.709 |  2621.936 |   574.227 |   0 | apis.google.com:443 ->  10.0.0.1:8000
 2047.862 |  2047.862 |     0.000 |   1 | apis.google.com:443 ->  nil
 2047.868 |  2047.868 |     0.000 |   1 | apis.google.com:443 ->  nil
 2403.425 |  2403.425 |     0.000 |   1 | apis.google.com:443 ->  nil
 2403.468 |  2403.468 |     0.000 |   1 | apis.google.com:443 ->  nil
 2403.474 |  2403.474 |     0.000 |   1 | apis.google.com:443 ->  nil
 2621.886 |  2621.886 |     0.000 |   1 | apis.google.com:443 ->  nil
 2621.929 |  2621.929 |     0.000 |   1 | apis.google.com:443 ->  nil
 2621.934 |  2621.934 |     0.000 |   1 | apis.google.com:443 ->  nil
 2759.549 |  2759.549 |     0.000 |   1 | www.google.com:80 ->  nil
 2759.574 |  2759.574 |     0.000 |   1 | www.google.com:80 ->  nil
 2770.227 |  2770.227 |     0.000 |   1 | www.google.com:80 ->  nil
 2770.253 |  2770.253 |     0.000 |   1 | www.google.com:80 ->  nil
 2792.693 |  2792.693 |     0.000 |   1 | www.google.com:80 ->  nil
 2792.721 |  2792.721 |     0.000 |   1 | www.google.com:80 ->  nil
 4801.288 |  4801.288 |     0.000 |   1 | www.google.com:80 ->  nil
 4801.317 |  4801.317 |     0.000 |   1 | www.google.com:80 ->  nil
 4805.463 |  4805.463 |     0.000 |   1 | www.google.com:80 ->  nil
 4805.488 |  4805.488 |     0.000 |   1 | www.google.com:80 ->  nil
</resolves>

<transactions>
strt (ms) | end (ms)  | len (ms)  | url
  162.246 |   970.350 |   808.104 | http://www.google.com/
  972.326 |  1191.690 |   219.364 | http://www.google.com/images/srpr/logo11w.png
  946.834 |  1470.182 |   523.348 | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png
 1194.637 |  1967.258 |   772.621 | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg
 1010.147 |  2169.635 |  1159.488 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
 2047.688 |  2833.271 |   785.583 | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0
 2759.529 |  3282.334 |   522.805 | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf.
 2792.668 |  4279.935 |  1487.267 | http://www.google.com/textinputassistant/tia.png
 2770.206 |  4295.328 |  1525.122 | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw
</transactions>

<responses>
status       | mime_type       | charset | url -> redirect_url
          OK |       text/html |         | http://www.google.com/ -> nil
          OK |       text/html |         | http://www.google.com/images/srpr/logo11w.png -> nil
          OK |       text/html |         | http://ssl.gstatic.com/gb/images/v1_b444d4f7.png -> nil
          OK |       text/html |         | http://www.gstatic.com/og/_/js/k=og.og.en_US.qkxEgtskWhU.O/rt=j/m=sy20,sy21,sy22,sy23,sy24,sy25,sy26,d,ld,sy31,gl,sy30,is,sy28,id,nb,nw,sb,sd,st,awd,p,vd,lod,eld,ip,sy32,dp,cpd/rs=AItRSTN3KHDZaT8PCaVk59gxEjUWC3npFg -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=c,sb,cr,jp,jsa,elog,r,hsm,j,p,pcc,csi/am=OIZ2Igs/rt=j/d=1/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
             |                 |         | https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.ZazSgj09RkI.O/m=gapi_iframes,googleapis_client,plusone/rt=j/sv=1/d=1/ed=1/rs=AItRSTOHQdxP80hcvThYZeDSZVUf0jtShw/cb=gapi.loaded_0 -> nil
          OK |       text/html |         | http://www.google.com/extern_chrome/7db8c505f16834e2.js?bav=on.2,or.r_qf. -> nil
          OK |       text/html |         | http://www.google.com/textinputassistant/tia.png -> nil
          OK |       text/html |         | http://www.google.com/xjs/_/js/k=xjs.s.en_US.9CjFb4DKbRI.O/m=sy21,cdos,sy40,gf,vm,tbui,sy53,sy106,sy218,cfm,sy26,sy54,sy85,sy25,sy48,sy51,sy55,sy86,sy97,sy63,sy67,sy90,sy87,sy28,sy140,sy98,sy99,sy46,sy64,sy66,sy68,sy89,sy91,sy88,sy141,sy142,sy143,sy94,sy96,sy52,sy65,sy92,sy139,sy144,sy145,sy146,sy147,sy148,actn,abd,foot,idck,ifl,sy114,sy42,sy115,sy81,sy231,sy235,lu,sy76,imap,sy238,m,me,sy70,sy157,sy101,sy152,sy154,sy156,sy153,spch,sy198,sy197,sy211,sy199,em3,em4,em5,sy250,tnv,sy149,adp,async,erh,sy107,hv,sy108,jsaleg,lc,sf,sy214,sfa/am=OIZ2Igs/rt=j/d=0/sv=1/rs=AItRSTNShv0CHUgINFOQreiDcuXJgts6iw -> nil
</responses>

<queries>
Collections of histograms for DNS.
Histogram: DNS.TotalTime recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 


Collections of histograms for Net.
Histogram: Net.Compress.NoProxy.ShouldHaveBeenCompressed recorded 8 samples, average = 153671.8 (flags = 0x1)
0       ------------------------------------------------------------------------O (1 = 12.5%)
500     ... 
12985   ------------------------------------------------------------------------O (1 = 12.5%) {12.5%}
14032   ... 
52461   ------------------------------------------------------------------------O (1 = 12.5%) {25.0%}
56692   ... 
71547   ------------------------------------------------------------------------O (1 = 12.5%) {37.5%}
77318   ... 
133075  ------------------------------------------------------------------------O (1 = 12.5%) {50.0%}
143809  ------------------------------------------------------------------------O (1 = 12.5%) {62.5%}
155408  ... 
364793  ------------------------------------------------------------------------O (1 = 12.5%) {75.0%}
394217  ------------------------------------------------------------------------O (1 = 12.5%) {87.5%}
426014  ... 

Histogram: Net.ConnectionTypeCount3 recorded 23 samples, average = 3.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 56.5%)
1  ... 
7  -------------------------------------------------------O                  (10 = 43.5%) {56.5%}
8  ... 

Histogram: Net.ConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.DNS_Resolution_And_TCP_Connection_Latency2 recorded 13 samples, average = 138.2 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (9 = 69.2%) {0.0%}
113  ... 
186  ----------------O                                                         (2 = 15.4%) {69.2%}
211  ----------------O                                                         (2 = 15.4%) {84.6%}
239  ... 

Histogram: Net.GoogleConnectionUsedSSLVersionFallback recorded 3 samples, average = 1.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 33.3%)
1  ------------------------------------------------------------------------O (1 = 33.3%) {33.3%}
2  ------------------------------------------------------------------------O (1 = 33.3%) {66.7%}
3  ... 

Histogram: Net.HadConnectionType3 recorded 2 samples, average = 3.5 (flags = 0x1)
0  ------------------------------------------------------------------------O (1 = 50.0%)
1  ... 
7  ------------------------------------------------------------------------O (1 = 50.0%) {50.0%}
8  ... 

Histogram: Net.HttpConnectionLatency recorded 10 samples, average = 136.5 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (7 = 70.0%) {0.0%}
113  ... 
186  ---------------------O                                                    (2 = 20.0%) {70.0%}
211  ----------O                                                               (1 = 10.0%) {90.0%}
239  ... 

Histogram: Net.HttpJob.TotalTime recorded 9 samples, average = 865.6 (flags = 0x1)
0     ... 
190   ------------------------O                                                 (1 = 11.1%) {0.0%}
226   ... 
449   ------------------------------------------------O                         (2 = 22.2%) {11.1%}
533   ... 
752   ------------------------------------------------------------------------O (3 = 33.3%) {33.3%}
894   O                                                                         (0 = 0.0%) {66.7%}
1062  ------------------------O                                                 (1 = 11.1%) {66.7%}
1262  ------------------------O                                                 (1 = 11.1%) {77.8%}
1500  ------------------------O                                                 (1 = 11.1%) {88.9%}
1782  ... 

Histogram: Net.HttpJob.TotalTimeCancel recorded 1 samples, average = 785.0 (flags = 0x1)
0    ... 
752  ------------------------------------------------------------------------O (1 = 100.0%) {0.0%}
894  ... 

Histogram: Net.HttpJob.TotalTimeNotCached recorded 8 samples, average = 875.6 (flags = 0x1)
0     ... 
190   ------------------------------------O                                     (1 = 12.5%) {0.0%}
226   ... 
449   ------------------------------------------------------------------------O (2 = 25.0%) {12.5%}
533   ... 
752   ------------------------------------------------------------------------O (2 = 25.0%) {37.5%}
894   O                                                                         (0 = 0.0%) {62.5%}
1062  ------------------------------------O                                     (1 = 12.5%) {62.5%}
1262  ------------------------------------O                                     (1 = 12.5%) {75.0%}
1500  ------------------------------------O                                     (1 = 12.5%) {87.5%}
1782  ... 

Histogram: Net.HttpJob.TotalTimeSuccess recorded 8 samples, average = 875.6 (flags = 0x1)
0     ... 
190   ------------------------------------O                                     (1 = 12.5%) {0.0%}
226   ... 
449   ------------------------------------------------------------------------O (2 = 25.0%) {12.5%}
533   ... 
752   ------------------------------------------------------------------------O (2 = 25.0%) {37.5%}
894   O                                                                         (0 = 0.0%) {62.5%}
1062  ------------------------------------O                                     (1 = 12.5%) {62.5%}
1262  ------------------------------------O                                     (1 = 12.5%) {75.0%}
1500  ------------------------------------O                                     (1 = 12.5%) {87.5%}
1782  ... 

Histogram: Net.HttpResponseCode recorded 8 samples, average = 200.0 (flags = 0x1)
0    ... 
200  ------------------------------------------------------------------------O (8 = 100.0%) {0.0%}
201  ... 

Histogram: Net.HttpSocketType recorded 10 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (10 = 100.0%)
1  ... 

Histogram: Net.HttpTimeToFirstByte recorded 9 samples, average = 484.8 (flags = 0x1)
0     ... 
171   ------------------------------------O                                     (1 = 11.1%) {0.0%}
210   ------------------------------------------------------------------------O (2 = 22.2%) {11.1%}
258   ------------------------------------------------------------------------O (2 = 22.2%) {33.3%}
317   ------------------------------------O                                     (1 = 11.1%) {55.6%}
389   O                                                                         (0 = 0.0%) {66.7%}
477   ------------------------------------O                                     (1 = 11.1%) {66.7%}
585   O                                                                         (0 = 0.0%) {77.8%}
718   ------------------------------------O                                     (1 = 11.1%) {77.8%}
881   ... 
1326  ------------------------------------O                                     (1 = 11.1%) {88.9%}
1627  ... 

Histogram: Net.NumDuplicateCookiesInDb recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.PreconnectUtilization2 recorded 11 samples, average = 2.0 (flags = 0x1)
0  ... 
2  ------------------------------------------------------------------------O (11 = 100.0%) {0.0%}
3  ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_ReusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCP recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketIdleTimeBeforeNextUse_UnusedSocket_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSL2 recorded 3 samples, average = 107.0 (flags = 0x1)
0    ... 
107  ------------------------------------------------------------------------O (3 = 100.0%) {0.0%}
108  ... 

Histogram: Net.SocketInitErrorCodes_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCP recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketInitErrorCodes_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCP recorded 13 samples, average = 138.2 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (9 = 69.2%) {0.0%}
113  ... 
186  ----------------O                                                         (2 = 15.4%) {69.2%}
211  ----------------O                                                         (2 = 15.4%) {84.6%}
239  ... 

Histogram: Net.SocketRequestTime_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketRequestTime_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_HTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SOCK recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSL2 recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLForProxies recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_SSLforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCP recorded 13 samples, average = 0.0 (flags = 0x1)
0  ------------------------------------------------------------------------O (13 = 100.0%)
1  ... 

Histogram: Net.SocketType_TCPforHTTPProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforHTTPSProxy recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.SocketType_TCPforSOCKS recorded 0 samples (flags = 0x1)
0 ... 

Histogram: Net.TCP_Connection_Latency recorded 13 samples, average = 138.2 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (9 = 69.2%) {0.0%}
113  ... 
186  ----------------O                                                         (2 = 15.4%) {69.2%}
211  ----------------O                                                         (2 = 15.4%) {84.6%}
239  ... 

Histogram: Net.TCP_Connection_Latency_IPv4_No_Race recorded 13 samples, average = 138.2 (flags = 0x1)
0    ... 
100  ------------------------------------------------------------------------O (9 = 69.2%) {0.0%}
113  ... 
186  ----------------O                                                         (2 = 15.4%) {69.2%}
211  ----------------O                                                         (2 = 15.4%) {84.6%}
239  ... 

Histogram: Net.Transaction_Latency_Total recorded 8 samples, average = 875.0 (flags = 0x1)
0     ... 
211   ------------------------------------O                                     (1 = 12.5%) {0.0%}
239   ... 
505   ------------------------------------------------------------------------O (2 = 25.0%) {12.5%}
572   ... 
734   ------------------------------------------------------------------------O (2 = 25.0%) {37.5%}
831   ... 
1065  ------------------------------------O                                     (1 = 12.5%) {62.5%}
1206  O                                                                         (0 = 0.0%) {75.0%}
1365  ------------------------------------------------------------------------O (2 = 25.0%) {75.0%}
1546  ... 

Histogram: Net.Transaction_Latency_Total_[21812:21817:8588256659:FATAL:url_request_context.cc(121)] Check failed: false. Leaked 2 URLRequest(s). First URL: http://www.google.com/images/nav_logo170.png.
New_Connection recorded 8 samples, average = 875.0 (flags = 0x1)
0     ... 
211   ------------------------------------O                                     (1 = 12.5%) {0.0%}
239   ... 
505   ------------------------------------------------------------------------O (2 = 25.0%) {12.5%}
572   ... 
734   ------------------------------------------------------------------------O (2 = 25.0%) {37.5%}
831   ... 
1065  ------------------------------------O                                     (1 = 12.5%) {62.5%}
1206  O                                                                         (0 = 0.0%) {75.0%}
1365  ------------------------------------------------------------------------O (2 = 25.0%) {75.0%}
1546  ... 

Histogram: Net.Transaction_Latency_b recorded 8 samples, average = 730.1 (flags = 0x1)
0     ... 
113   ------------------------------------O                                     (1 = 12.5%) {0.0%}
128   ... 
307   ------------------------------------O                                     (1 = 12.5%) {12.5%}
348   O                                                                         (0 = 0.0%) {25.0%}
394   ------------------------------------O                                     (1 = 12.5%) {25.0%}
446   O                                                                         (0 = 0.0%) {37.5%}
505   ------------------------------------O                                     (1 = 12.5%) {37.5%}
572   ------------------------------------O                                     (1 = 12.5%) {50.0%}
648   ... 
941   ------------------------------------O                                     (1 = 12.5%) {62.5%}
1065  ... 
1365  ------------------------------------------------------------------------O (2 = 25.0%) {75.0%}
1546  ... 


</queries>
Received signal 6
 [0x000000531f1e] base::debug::StackTrace::StackTrace()
 [0x000000532418] base::debug::(anonymous namespace)::StackDumpSignalHandler()
 [0x7f591102fbd0] <unknown>
 [0x7f5910059037] gsignal
 [0x7f591005c698] abort
 [0x00000056c349] base::debug::BreakDebugger()
 [0x0000005405cd] logging::LogMessage::~LogMessage()
 [0x000001c2eb7e] net::URLRequestContext::AssertNoURLRequests()
 [0x000001c2ebc8] net::URLRequestContext::~URLRequestContext()
 [0x000001aec899] TestShellRequestContext::~TestShellRequestContext()
 [0x000001ae3c1f] (anonymous namespace)::IOThread::CleanUp()
 [0x00000055eafd] base::Thread::ThreadMain()
 [0x00000055e361] base::(anonymous namespace)::ThreadFunc()
 [0x7f5911027f8e] start_thread
 [0x7f591011be1d] clone
  r8: 000000000204024d  r9: 00007f5909885b26 r10: 0000000000000008 r11: 0000000000000202
 r12: 0000000000000000 r13: 00007f59103e4848 r14: 00007fffa5fa34e0 r15: 0000000000001000
  di: 0000000000005534  si: 0000000000005539  bp: 00007f5910c074c0  bx: 00007f5909886620
  dx: 0000000000000006  ax: 0000000000000000  cx: ffffffffffffffff  sp: 00007f5909885dc8
  ip: 00007f5910059037 efl: 0000000000000202 cgf: 0000000000000033 erf: 0000000000000000
 trp: 0000000000000000 msk: 0000000000000000 cr2: 0000000000000000
